<?php
namespace Magenest\CountDown\Block\Adminhtml\CountDown;

use Magenest\CountDown\Model\ResourceModel\CountDown\CollectionFactory;
use Magenest\CountDown\Helper\CountDownData;
use \Magento\Backend\Block\Template;
class ChangeColor extends Template{

    private $collectionFactory;
    private $countDownData;


    public function __construct(
        CountDownData $countDownData,
        Template\Context $context,
        array $data = [],
        CollectionFactory $collectionFactory
    )
    {
        $this->countDownData = $countDownData;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    public function getColorText(){
        $titleClock = $this->countDownData->getConfigClock("color_text");
        return $titleClock;
    }
}
