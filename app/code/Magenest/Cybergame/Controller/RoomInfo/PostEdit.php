<?php
namespace Magenest\Cybergame\Controller\RoomInfo;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magenest\Cybergame\Model\RoomExtraOptionFactory;
use Magento\Framework\Controller\ResultFactory;

class PostEdit extends Action{

    private $pageFactory;
    private $roomExtraOptionFactory;

    public function __construct(
        Context $context,
        RoomExtraOptionFactory $roomExtraOptionFactory
    )
    {
        $this->roomExtraOptionFactory = $roomExtraOptionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();

        $room = $this->roomExtraOptionFactory->create()->load($post['id']);
        $room->setNumberPc($post['number_pc']);
        $room->setAddress($post['address']);
        $room->setFoodPrice($post['food_price']);
        $room->setDrinkPrice($post['drink_price']);
        $room->setUpdateAt(date('Y-m-d H:i:s'));
        $room->save();

        $this->messageManager->addSuccess("Update Successfully");
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $redirect->setPath("*/*/update");
    }
}
