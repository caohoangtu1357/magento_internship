<?php

namespace Magenest\Movie\Block\Customer;

use Magento\Framework\View\Element\Template;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\App\Http;

class CustomTab extends Template
{
    private $customerFactory;
    private $request;
    private $customerSession;
    private $storeManager;

    public function __construct(Template\Context $context,
                                \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
                                array $data = [],
                                CustomerFactory $customerFactory,
                                Http $request,
                                \Magento\Customer\Model\SessionFactory $customerSession,
                                \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        parent::__construct($context, $data);
        $this->storeManager=$storeManager;
        $this->customerSession=$customerSession;
        $this->request=$request;
        $this->customerFactory=$customerFactory;
    }

    public function getCustomerInfo(){
        $baseUrl=$this->storeManager->getStore()->getBaseUrl();
        $customerModel=$this->customerFactory->create();
        $customer=$this->customerSession->create();
        $id=$customer->getCustomer()->getData('entity_id');
        $customerObj= $customerModel->load($id);
        $url=$customerObj->getCertificateUrl();
        $customerData=$customerObj->getData();
        if(isset($customerData['avatar'])){
            $avatar = $customerData['avatar'];
            $customerData['avatar']=$url;
        }else{
            $customerData['avatar']=$baseUrl."pub/media/customer/default.jpg";
        }

        return $customerData;
    }



}
