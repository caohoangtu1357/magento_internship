define(['jquery'],function($){
    'use strict';
    return function handlerChangeQty(){


        var price = $('meta[itemprop=price]').attr("content");
        var currencySymbol = $('#currency_symbol').val();
        var idProduct= $("input[name=item]").val();

        $('#qty').change(function(){
            var qty = $('#qty').val();
            var totalPrice=(parseInt(price)*parseInt(qty));
            $(`#product-price-${idProduct} .price`)[0].innerText= currencySymbol+(Math.round(totalPrice * 100) / 100).toFixed(2);
        });

    }
})
