define(['jquery','Magento_Ui/js/modal/alert'],function($,alert){
    "use strict"
    return function manufacturer(){
        $("#manufacturer").click(function(){
            var id = $("input[name='item']").val();

            var urlGet= $('#url_get_manufacturer').val()+"id/"+id;
            $.get(urlGet,function(res){

                alert({
                    title: 'Manufacturer Information',
                    content: `<p><b>Name:</b> ${res.name} </p>` +
                        `<p><b>Street Address:</b> ${res.address_street} </p>` +
                        `<p><b>City:</b> ${res.address_country} </p>` +
                        `<p><b>Contact name:</b> ${res.contact_name} </p>` +
                        `<p><b>Phone:</b> ${res.contact_phone} </p>`
                });
            });


        })
    }
})
