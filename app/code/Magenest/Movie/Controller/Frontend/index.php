<?php
namespace Magenest\Movie\Controller\Frontend;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;

class Index extends \Magento\Framework\App\Action\Action{

    protected $pageFactory;

    public function __construct(Context $context,\Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->pageFactory=$pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $pageResult = $this->pageFactory->create();

        return $pageResult;
    }
}
