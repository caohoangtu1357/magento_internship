<?php
namespace Magenest\RestAPI\Model;

use Magenest\RestAPI\Api\ActorRepositoryInterface;
use Magenest\RestAPI\Api\Data;
use Magenest\RestAPI\Api\Data\ActorInterface;
use Magenest\RestAPI\Model\ActorFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class ActorRepository implements ActorRepositoryInterface{

    private $actor;
    private $actorFactory;
    private $request;
    private $jsonFactory;

    public function __construct(
        ActorInterface $actor,
        ActorFactory $actorFactory,
        \Magento\Framework\Webapi\Rest\Request $request,
        JsonFactory $jsonFactory
    ){
        $this->request = $request;
        $this->actor = $actor;
        $this->actorFactory = $actorFactory;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function get()
    {
        $id = $this->request->getParam('id');
        $actor = $this->actor->get($id);
        return $actor;
    }


    public function update()
    {
        $actor = $this->actor->load($this->request->getParam('id'));
        $actor->setName($this->request->getBodyParams()['name']);

        return $actor->save();
    }

    public function deleteById($id)
    {
        $this->actor->load($id)->delete();
        return "deleted";
    }

    public function createActor()
    {
        $newActor = $this->actor->setName($this->request->getBodyParams()['name']);
        return $newActor->save();
    }
}
