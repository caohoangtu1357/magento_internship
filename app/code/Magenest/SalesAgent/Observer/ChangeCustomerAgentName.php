<?php
namespace Magenest\SalesAgent\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ChangeCustomerAgentName implements ObserverInterface{


    public function __construct(
    ){
    }

    public function execute(Observer $observer)
    {

        $customerData=$observer->getCustomer()->getData();
        if(isset($customerData['is_sales_agent'])){
            if($customerData['is_sales_agent']==1){
                $observer->getCustomer()->setFirstname("Sales Agent: ".$observer->getCustomer()->getFirstname());
            }
        }
        return $observer;

    }
}
