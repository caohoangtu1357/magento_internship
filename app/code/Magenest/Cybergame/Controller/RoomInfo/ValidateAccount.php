<?php
namespace Magenest\Cybergame\Controller\RoomInfo;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magenest\Cybergame\Model\ResourceModel\GamerAccountList\CollectionFactory;

class ValidateAccount extends Action{

    private $jsonFactory;
    private $collectionFactory;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory=$collectionFactory;
        $this->jsonFactory=$jsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $accountName = $this->getRequest()->getParam("name");
        $collection = $this->collectionFactory->create();
        $a=0;
        $account = $collection->addFieldToSelect("*")->addFieldToFilter("account_name",$accountName)
            ->getData();
        $result = ["status"=>"notFound"];
        if(count($account)>0){
            $result = ["status"=>"found"];
        }

        $json = $this->jsonFactory->create();
        return $json->setData($result);
    }
}
