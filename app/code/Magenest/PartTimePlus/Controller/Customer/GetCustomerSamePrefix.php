<?php
namespace Magenest\PartTimePlus\Controller\Customer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Magento\Customer\Model\SessionFactory;
use Magento\Customer\Model\CustomerFactory;

class GetCustomerSamePrefix extends Action{

    private $jsonFactory;
    private $collectionFactory;
    private $sessionFactory;
    private $customerFactory;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        CollectionFactory $collectionFactory,
        SessionFactory $sessionFactory,
        CustomerFactory $customerFactory
    )
    {
        $this->sessionFactory=$sessionFactory;
        $this->collectionFactory = $collectionFactory;
        $this->jsonFactory=$jsonFactory;
        $this->customerFactory= $customerFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $customerId = $this->sessionFactory->create()->getCustomer()->getId();
        $customerCustomPrefix = $this->customerFactory->create()->load($customerId)->getCustomPrefix();

        $customerCollection = $this->collectionFactory->create();
        $customersSanePrefix = $customerCollection
            ->addFieldToFilter("entity_id",["neq"=>$customerId])
            ->addAttributeToFilter("custom_prefix",$customerCustomPrefix)
            ->addAttributeToSelect("custom_prefix")
            ->addFieldToSelect("*")->getData();

        $jsonRes = $this->jsonFactory->create()->setData($customersSanePrefix);
        return $jsonRes;
    }
}
