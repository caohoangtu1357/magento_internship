<?php

namespace Magenest\HoangTu\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class HotelOption extends AbstractSource{


    public function getRoomTypes(){
        $roomTypes=[
            ["name"=>"single","value"=>"single"],
            ["name"=>"double","value"=>"double"],
            ["name"=>"triple","value"=>"triple"]
        ];
        return $roomTypes;
    }

    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options=[];


            $roomTypes=$this->getRoomTypes();
            foreach ($roomTypes as $roomType){
                array_push($this->_options,[
                    'value'=>$roomType['value'],
                    'label'=>$roomType['name']
                ]);
            }

        }
        return $this->_options;
    }

    final public function toOptionArray()
    {
        $options=[];
        $roomTypes=$this->getRoomTypes();
        foreach ($roomTypes as $roomType){
            array_push($options,[
                'value'=>$roomType['value'],
                'label'=>$roomType['name']
            ]);
        }

        return $options;
    }
}
