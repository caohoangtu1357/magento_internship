<?php
namespace Magenest\HoangTu\Controller\Vendor;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class CheckExistVendor extends Action{

    private $pageFactory;
    private $collectionFactory;
    private $jsonFactory;

    public function __construct(
        Context $context,
        \Magenest\HoangTu\Model\ResourceModel\Vendor\CollectionFactory $collectionFactory,
        JsonFactory $jsonFactory
    )
    {
        $this->collectionFactory=$collectionFactory;
        $this->jsonFactory=$jsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $email = $this->getRequest()->getParams()['email'];
        $collection=$this->collectionFactory->create();
        $collection->addFieldToSelect("email")->addFieldToFilter("email",$email);
        $data=$collection->getData();

        $json=$this->jsonFactory->create();

        if(count($data)==0){
            $json->setData(["status"=>"notFound"]);
        }else{
            $json->setData(["status"=>"found"]);
        }
        return $json;
    }
}
