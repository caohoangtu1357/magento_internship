<?php
namespace Magenest\JuniorChap5\Ui\Component\Listing\Column;

use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Framework\Api\SearchCriteriaBuilder;
use \Magento\Catalog\Model\ProductFactory;

class TypeVarchar extends Column
{
    protected $_searchCriteria;
    private $productFactory;
    private $authSession;
    private $objectManager;

    public function __construct(
        ProductFactory $productFactory,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        SearchCriteriaBuilder $criteria,
        \Magento\Backend\Model\Auth\Session $authSession,
        array $components = [],
        array $data = []
    )
    {
        $this->authSession = $authSession;
        $this->productFactory = $productFactory;
        $this->_searchCriteria = $criteria;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {

        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {

                $product = $this->productFactory->create();

                $product=$product->load($item["entity_id"]);

                $item["type_varchar"]=$product->getTypeVarchar()?$product->getTypeVarchar():"";

                $item[$this->getData('name')] = html_entity_decode($item["type_varchar"]);
            }
        }
        return $dataSource;
    }

    public function prepare(){
        parent::prepare();
        $firstname = $this->authSession->getUser()->getFirstname();

        $firstChar= strtoupper($firstname[0]);

        if(strcmp($firstChar,"A")>=0 && strcmp($firstChar,"M")<=0){
            $this->_data['config']['componentDisabled'] = false;
        }else{
            $this->_data['config']['componentDisabled'] = true;
        }
    }
}
