<?php
namespace Magenest\PartTimePlus\Plugin;
use Magento\Framework\App\Request\Http;
use Magento\Checkout\Model\Cart;
use Magento\Framework\Controller\Result\RedirectFactory;

class PluginCheckoutController{

    private $http;
    private $cart;
    private $redirectFactory;

    public function __construct(
        Http $http,
        Cart $cart,
        RedirectFactory $redirectFactory
    ){
        $this->redirectFactory=$redirectFactory;
        $this->cart=$cart;
        $this->http=$http;
    }

    public function aroundExecute($subject, $process){
        $resourceItems=$this->cart->getItems()->getItems();

        if(count($resourceItems)==1){
            foreach ($resourceItems as $item){
                if($item->getQty()==1){
                    return $process();
                }
            }
        }
        $redirect =$this->redirectFactory->create();
        return $redirect->setPath("/");
    }
}
