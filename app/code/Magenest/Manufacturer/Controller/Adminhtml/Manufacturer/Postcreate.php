<?php
namespace Magenest\Manufacturer\Controller\Adminhtml\Manufacturer;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magenest\Manufacturer\Model\ManufacturerFactory;

class Postcreate extends \Magento\Backend\App\Action{

    private $manufacturerFactory;

    public function __construct(
        Action\Context $context,
        ManufacturerFactory $manufacturerFactory
    )
    {
        $this->manufacturerFactory=$manufacturerFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $post=$this->getRequest()->getPostValue();

        $postValue = new \Magento\Framework\DataObject($post);

        $manufacturer=$this->manufacturerFactory->create();
        $manufacturer->setName($post['name']);
        if(isset($post['enabled'])){
            $manufacturer->setEnabled(1);
        }else{
            $manufacturer->setEnabled(0);
        }
        $manufacturer->setAddressStreet($post['address_street']);
        $manufacturer->setAddressCity($post['address_city']);
        $manufacturer->setAddressCountry($post['address_country']);
        $manufacturer->setContactName($post['contact_name']);
        $manufacturer->setContactPhone($post['contact_phone']);
        $manufacturer->save();

        $this->messageManager->addSuccess(__('Create Success'));
        $resultRedirect=$this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');

    }

}
