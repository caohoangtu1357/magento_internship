<?php
namespace Magenest\SalesAgent\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magenest\SalesAgent\Model\SalesAgentFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Sales\Model\OrderFactory;


class AddSalesAgent implements ObserverInterface{

    private $salesAgentFactory;
    private $productFactory;
    private $orderFactory;

    public function __construct(
        SalesAgentFactory $salesAgentFactory,
        ProductFactory $productFactory,
        OrderFactory $orderFactory
    ){
        $this->orderFactory=$orderFactory;
        $this->productFactory=$productFactory;
        $this->salesAgentFactory=$salesAgentFactory;
    }

    public function execute(Observer $observer)
    {
        $order = $this->orderFactory->create();
        $items = $order->load($observer->getData("order_ids")[0])->getAllVisibleItems();

        foreach ($items as $item){
            $itemData = $item->getData();
            $product = $this->productFactory->create()->load($itemData['product_id'])->getOrigData();
            $salesAgent = $this->salesAgentFactory->create();
            $salesAgent->setOrderId($itemData['order_id']);
            $salesAgent->setOrderItemId($itemData['product_id']);
            $salesAgent->setOrderItemSku($itemData['sku']);
            $salesAgent->setOrderItemPrice($itemData['price']);
            if(isset($product['commission_value'])){
                $salesAgent->setCommissionValue($product['commission_value']);
                if($product['commission_type']=="percent"){
                    $salesAgent->setCommisionPercent($product['commission_value']);
                }else{
                    $salesAgent->setCommisionPercent(0);
                }
                $salesAgent->save();
            }

        }
        return $observer;

    }
}
