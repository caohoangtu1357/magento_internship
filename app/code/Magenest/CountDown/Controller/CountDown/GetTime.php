<?php

namespace Magenest\CountDown\Controller\CountDown;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magenest\CountDown\Model\ResourceModel\CountDown\CollectionFactory;

class GetTime extends Action{

    private $jsonFactory;
    private $collectionFactory;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->jsonFactory = $jsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $countDownCollection = $this->collectionFactory->create();
        $countDown = $countDownCollection->addFieldToFilter("status","enable")->getData()[0];
        $time = $countDown['countdown_value'];
        return $this->jsonFactory->create()->setData( $time);
    }
}
