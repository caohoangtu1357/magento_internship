<?php

namespace Magenest\CountDown\Block\Adminhtml\FormConfig;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magenest\CountDown\Model\Config\Source\ClockType;
use Magenest\CountDown\Model\Config\Source\CustomerGroup;
use Magenest\JuniorChap3\Model\Config\Source\Color;
/**
 * Class Ranges
 */
class ClockTypes extends AbstractFieldArray
{
    /**
     * @var ClockType
     */
    private $clockTypeRenderer;

    /**
     * @var CustomerGroup
     */
    private $customerGroupRenderer;

    /**
     * @var Color
     */
    private $colorRenderer;


    /**
     * Prepare rendering the new field by adding all the needed columns
     */
    protected function _prepareToRender(): void
    {
        $this->addColumn('customer_group', [
            'label' => __('Customer group'),
            'class' => 'required-entry',
            'renderer' => $this->getCustomerGroupRenderer(),
            ]);
        $this->addColumn('clock_type', [
            'label' => __('Clock Type'),
            'class' => 'required-entry',
            'renderer' => $this->getClockTypeRenderer()
        ]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    protected function _prepareArrayRow(DataObject $row): void
    {
        $options = [];

        $clockType = $row->getClockType();
        if ($clockType !== null) {
            $options['option_' . $this->getClockTypeRenderer()->calcOptionHash($clockType)] = 'selected="selected"';
        }

        $customerGroup = $row->getCustomerGroup();
        if ($customerGroup !== null) {
            $options['option_' . $this->getCustomerGroupRenderer()->calcOptionHash($customerGroup)] = 'selected="selected"';
        }

        $row->setData('option_extra_attrs', $options);
    }

    private function getClockTypeRenderer()
    {
        if (!$this->clockTypeRenderer) {
            $this->clockTypeRenderer = $this->getLayout()->createBlock(
                ClockType::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->clockTypeRenderer;
    }


    private function getCustomerGroupRenderer()
    {
        if (!$this->customerGroupRenderer) {
            $this->customerGroupRenderer = $this->getLayout()->createBlock(
                CustomerGroup::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->customerGroupRenderer;
    }

}
