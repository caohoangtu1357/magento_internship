<?php

namespace Magenest\AddToCartCookie\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Checkout\Model\Cart;

class AddCartCookie implements ObserverInterface{

    const COOKIE_NAME = 'num_item_cart';

    private $cookieManager;

    private $cookieMetadataFactory;

    private $sessionManager;

    private $cart;

    public function __construct(
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        SessionManagerInterface $sessionManager,
        Cart $cart
    ){
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionManager = $sessionManager;
        $this->cart = $cart;
    }

    public function get()
    {
        return $this->cookieManager->getCookie(self::COOKIE_NAME);
    }

    public function execute(Observer $observer)
    {
        $cartItems = $this->cart->getQuote()->getItemsCount();

        $metadata = $this->cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration(3600)
            ->setPath($this->sessionManager->getCookiePath())
            ->setDomain($this->sessionManager->getCookieDomain());

        $this->cookieManager->setPublicCookie(
            self::COOKIE_NAME,
            $cartItems,
            $metadata
        );

    }
}
