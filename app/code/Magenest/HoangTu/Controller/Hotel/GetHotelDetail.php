<?php

namespace Magenest\HoangTu\Controller\Hotel;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magenest\HoangTu\Model\ResourceModel\Hotel\CollectionFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class GetHotelDetail extends Action{

    private $hotelFactory;
    private $jsonFactory;

    public function __construct(
        Context $context,
        CollectionFactory $hotelFactory,
        JsonFactory $jsonFactory
    )
    {
        $this->jsonFactory=$jsonFactory;
        $this->hotelFactory=$hotelFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $hotel = $this->hotelFactory->create();
        $hotel->addFieldToSelect('*');
        $hotel->addFieldToFilter("hotel_id",$params['id']);
        $data = $hotel->getData();
        $json=$this->jsonFactory->create();
        return $json->setData($data[0]);
    }
}
