<?php

namespace Magenest\JuniorChap8\Plugin;

class SpecialPrice{
    public function aroundGetName($subject,$proceed){
        if($subject->getSpecialPrice()){
            $to = ($subject->getSpecialToDate());
            if($today = date("Y-m-d H:i:s") < $to){
                return "Special: ".$subject->getData()['name'];
            }

        }
        return $proceed();
    }
}
