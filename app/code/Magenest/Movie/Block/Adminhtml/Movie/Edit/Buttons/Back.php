<?php
namespace Magenest\Movie\Block\Adminhtml\Movie\Edit\Buttons;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Layout\Generic;

class Back extends Generic implements ButtonProviderInterface
{
    protected $url;
    public function __construct(
        UiComponentFactory $uiComponentFactory,
        $data = [],
        \Magento\Framework\UrlInterface $url
    )
    {
        $this->url=$url;
        parent::__construct($uiComponentFactory, $data);
    }

    /**
     * get button data
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'on_click' => sprintf("location.href = '%s';", $this->getBackUrl()),
            'class' => 'back',
            'sort_order' => 10
        ];
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->url->getUrl('*/*/');
    }
}
