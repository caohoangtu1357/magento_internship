<?php

namespace Magenest\Cybergame\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Sales\Model\OrderFactory;
use Magenest\Cybergame\Model\GamerAccountListFactory;
use Magento\Framework\Serialize\Serializer\Json;

class ObserverCheckout implements ObserverInterface{

    private $encryptor;
    private $orderFactory;
    private $gamerAccountListFactory;
    private $json;

    public function __construct(
        EncryptorInterface $encryptor,
        OrderFactory $orderFactory,
        GamerAccountListFactory $gamerAccountListFactory,
        Json $json
    ){
        $this->json=$json;
        $this->gamerAccountListFactory=$gamerAccountListFactory;
        $this->orderFactory=$orderFactory;
        $this->encryptor=$encryptor;
    }

    public function createAccount(array $options){
        $account = $this->gamerAccountListFactory->create();
        if(isset($options['account_name'])){
            $account->setAccountName($options['account_name']);
            $account->setProductId($options['item']);
            $account->setPassword($this->encryptor->encrypt("1"));
            $account->setHour($options['qty']);
            $account->save();
        }
    }

    public function execute(Observer $observer)
    {

        $order=$this->orderFactory->create();
        $items = $order->load($observer->getData("order_ids")[0])->getAllVisibleItems();

        foreach ($items as $item){
            $itemData = $item->getOrigData("product_options");
            $data = $this->json->unserialize($itemData);
            $options = $data["info_buyRequest"];
            $this->createAccount($options);
        }

        return $observer;

    }
}
