<?php
namespace Packt\HelloWorld\Observer;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class RegisterVisitObserver implements ObserverInterface{

    protected $logger;

    public function __construct(\Psr\Log\LoggerInterface $logger){
        $this->logger=$logger;
    }

    public function execute(Observer $observer)
    {

        $var1=$observer->getData("var1");
        $this->logger->debug('Registered Event pa hon');
    }
}
