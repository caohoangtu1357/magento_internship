<?php
namespace Magenest\CountDown\Block\CountDown;

use Magento\Framework\View\Element\Template;
use Magenest\CountDown\Model\ResourceModel\CountDown\CollectionFactory;
use Magenest\CountDown\Helper\CountDownData;

class CountDownHeader extends Template{

    private $collectionFactory;
    private $countDownData;

    public function __construct(
        CountDownData $countDownData,
        Template\Context $context,
        array $data = [],
        CollectionFactory $collectionFactory
    )
    {
        $this->countDownData = $countDownData;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    public function getClockColor(){
        $colorClock = $this->countDownData->getConfigClock("color_clock");
        return $colorClock;
    }

    public function getClockTitle(){
        $titleClock = $this->countDownData->getConfigClock("title_clock");
        return $titleClock;
    }

    public function getColorText(){
        $titleClock = $this->countDownData->getConfigClock("color_text");
        return $titleClock;
    }
}
