<?php
namespace Magenest\Staff\Controller\Customer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magenest\Staff\Model\StaffFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class BeAStaff extends Action{

    private $staffFactory;
    private $jsonFactory;

    public function __construct(
        Context $context,
        StaffFactory $staffFactory,
        JsonFactory $jsonFactory
    )
    {
        $this->jsonFactory=$jsonFactory;
        $this->staffFactory=$staffFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $post= $this->getRequest()->getPostValue();
        $staff = $this->staffFactory->create();
        $staffData = $staff->load($post['idCustomer'],"customer_id")->getData();

        if(count($staffData)==0){
            $staff = $this->staffFactory->create();
        }

        $staff->setCustomerId($post["idCustomer"]);
        $staff->setNickName($post["nick_name"]);
        $staff->setType($post["type"]);
        $staff->setStatus(2);
        $staff->setUpdateAt(date("Y-m-d h:i:sa"));
        $staff->save();

        $json = $this->jsonFactory->create();
        $json->setData(["status"=>"success"]);
        return $json;
    }
}
