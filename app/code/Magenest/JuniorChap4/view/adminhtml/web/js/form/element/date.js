define([
    'jquery',
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/date',
    'Magento_Ui/js/modal/modal',
    'mage/url'
], function ($, _, uiRegistry, date, modal, url) {
    'use strict';
    return date.extend({
        defaults:{
            options:{
                beforeShowDay: function(d) {
                    if(d.getDate()<8 || d.getDate()>12){
                        return [false,"","UnAvailable"]
                    }else{
                        return [true,"","Available"]
                    }
                }
            }
        }
    })
});


