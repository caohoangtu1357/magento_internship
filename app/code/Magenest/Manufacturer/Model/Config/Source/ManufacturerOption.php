<?php
namespace Magenest\Manufacturer\Model\Config\Source;


class ManufacturerOption extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    private $collectionFactory;

    public function __construct(
        \Magenest\Manufacturer\Model\ResourceModel\Manufacturer\CollectionFactory $collectionFactory
    ){
        $this->collectionFactory=$collectionFactory;
    }

    protected $_options;

    /**
     * getAllOptions
     *
     * @return array
     */

    public function getManufacturers(){
        $manufacturer=$this->collectionFactory->create()->addFieldToSelect('*');
        $manufacturers=$manufacturer->getData();
        return $manufacturers;
    }

    public function getAllOptions()
    {

        if ($this->_options === null) {
            $this->_options=[];


            $manufacturers=$this->getManufacturers();
            foreach ($manufacturers as $manufacturer){
                array_push($this->_options,[
                    'value'=>$manufacturer['entity_id'],
                    'label'=>$manufacturer['name']
                ]);
            }

        }
        return $this->_options;
    }
    final public function toOptionArray()
    {
        $options=[];
        $manufacturers=$this->getManufacturers();
        foreach ($manufacturers as $manufacturer){
            array_push($options,[
                'value'=>$manufacturer['entity_id'],
                'label'=>$manufacturer['name']
            ]);
        }

        return $options;
    }
}
