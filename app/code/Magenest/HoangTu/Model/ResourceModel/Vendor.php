<?php
namespace Magenest\HoangTu\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Vendor extends AbstractDb{

    protected function _construct()
    {
        $this->_init("magenest_test_vendor_hoang_tu","id");
    }

    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        return parent::load($object, $value, $field);
    }
}
