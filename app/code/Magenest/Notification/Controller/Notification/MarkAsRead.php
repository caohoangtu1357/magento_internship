<?php
namespace Magenest\Notification\Controller\Notification;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Customer\Model\SessionFactory;
use Magento\Customer\Model\CustomerFactory;

class MarkAsRead extends Action{

    private $jsonFactory;
    private $sessionFactory;
    private $customerFactory;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        SessionFactory $sessionFactory,
        CustomerFactory $customerFactory
    )
    {
        $this->customerFactory = $customerFactory;
        $this->sessionFactory = $sessionFactory;
        $this->jsonFactory=$jsonFactory;
        parent::__construct($context);
    }

    public function getIdCustomer(){
        return $this->sessionFactory->create()->getCustomer()->getId();
    }

    public function updateViewedNote($idCustomer,$idNote){
        $customer = $this->customerFactory->create()->load($idCustomer);
        $vieweds = $customer->getNotificationViewed();
        if($vieweds){
            $viewedsArr = explode(",",$vieweds);
            array_push($viewedsArr,$idNote);
            $customer->setNotificationViewed(implode(",",$viewedsArr));
            $customer->save();
        }
    }

    public function execute()
    {
        $idNote = $this->getRequest()->getParam("id");
        $idCustomer = $this->getIdCustomer();
        $this->updateViewedNote($idCustomer,$idNote);
        $json = $this->jsonFactory->create();
        $json->setData(["status"=>"success"]);
        return $json;
    }
}
