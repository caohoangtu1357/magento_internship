<?php

namespace Magenest\HoangTu\Block\Product\Hotel;

use Magento\Framework\View\Element\Template;
use Magenest\HoangTu\Model\ResourceModel\Hotel\CollectionFactory;
use Magento\Catalog\Model\ProductFactory;

class RoomType extends Template{

    private $collectionFactory;
    private $productFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        CollectionFactory $collectionFactory,
        ProductFactory $productFactory
    )
    {
        $this->productFactory=$productFactory;
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context, $data);
    }

    public function getRoomType(){
        $productId=$this->getRequest()->getParam("id");

        $product = $this->productFactory->create()->load($productId);
        if(isset($product->getOrigData()['roomtype'])){
            $roomType = $product->getOrigData()['roomtype'];
            return $roomType;
        }else{
            return null;
        }
    }

    public function getHotels(){
        $hotel=$this->collectionFactory->create()->load();
        $hotels = $hotel->getData();
        return $hotels;
    }

    public function getUrlRoomDetailAjax(){
        return $this->getUrl("hoang_tu/hotel/gethoteldetail");
    }
}
