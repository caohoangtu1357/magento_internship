<?php

namespace Magenest\Staff\Controller\Staff;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magenest\Staff\Model\ResourceModel\Staff\CollectionFactory;

class GetSearch extends Action{

    private $collectionFactory;
    private $jsonFactory;

    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        JsonFactory $jsonFactory
    )
    {
        $this->jsonFactory =$jsonFactory;
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $keyWord = $this->getRequest()->getParam("key_word");
        $collection = $this->collectionFactory->create();
        $staffs = $collection->addFieldToFilter("nick_name",["like"=>"%$keyWord%"])
            ->getData();
        $json = $this->jsonFactory->create();
        $json->setData($staffs);
        return $json;
    }
}
