define(['jquery', 'uiComponent', 'ko'], function($, Component, ko) {

    var notification =function(item) {
        this.name = ko.observable(item.name);
        this.entity_id = ko.observable(item.entity_id);
        this.short_description = ko.observable(item.short_description);
        this.created_at = ko.observable(item.created_at);
        this.redirect_url = ko.observable(item.redirect_url);
        this.viewed = ko.observable(item.viewed);
        return this;
    };

    return Component.extend({



        initialize:function(){
            this._super();

            this.input1=ko.observable("input 1");
            this.input2=ko.observable("input 2");
            this.notifications = ko.observableArray();
            this.numNotification = ko.observable(0);

            this.getNumNotification= ko.pureComputed(function(){
                return this.numNotification();
            },this);


            this.dataInput= ko.pureComputed(function(){
                console.log(this.input1());
                return this.input1() + this.input2();
            },this);
        },

        getClass:function (data){
            if(data.viewed()){
                return "";
            }
            return "not-viewed";
        },

        isVisible:function(data){
            if(data.viewed()){
                return "not-visible";
            }else{
                return "visible btn-mark-as-read";
            }
        },

        markAsRead:function(data){

            var self=this;
            $.ajax({
                type: 'GET',
                url: '../markasread/id/'+data.entity_id(),
                context: this,
                success: function(res) {
                    var oldRead = $("#number-notification").text();
                    $("#number-notification").text(parseInt($("#number-notification").text())-1);
                    self.getNotifications();
                },
                dataType: 'json'
            });
            return "";
        },

        getTemplate: function () {
            return this.template;
        },

        deleteItem: function(data){
            var self=this;
            $.ajax({
                type: 'GET',
                url: '../deletenotification/id/'+data.entity_id(),
                context: this,
                success: function(res) {
                    var oldRead = $("#number-notification").text();
                    $("#number-notification").text(parseInt($("#number-notification").text())-1);
                    self.getNotifications();
                },
                dataType: 'json'
            });
            return "";
        },

        getNotifications:function(){
            var self=this;
            $.ajax({
                type: 'GET',
                url: '../getcustomernotification',
                context: this,
                success: function(res) {
                    console.log(res);

                    self.notifications($.map(res,function (item){
                        return new notification(item);
                    }));
                },
                dataType: 'json'
            });
        }
    });
});
