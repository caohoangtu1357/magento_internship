define(['jquery','Magento_Ui/js/modal/alert'],function($,alert){
    'use strict';
    return function ajaxUpdateUser(){



        $("#btn-update").click(function(){
            var type = $("#type").val();
            var nick_name = $("#nick_name").val();
            var idCustomer = $("#id-customer").val();
            var url=$("#updateUrl").val();
            $.post(url,
                {
                    idCustomer:idCustomer,
                    nick_name:nick_name,
                    type:type
                },function(res){
                    if(res.status=="success"){
                        alert({
                            "title":"Be A Staff",
                            "content":"Be A Staff Success"
                        });
                    }else{
                        alert({
                            "title":"Be A Staff",
                            "content":"Be A Staff Failure"
                        });
                    }
            });
        });
    }
})
