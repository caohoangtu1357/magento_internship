<?php
namespace Magenest\Movie\Plugin;

use Magento\Customer\Model\CustomerFactory;

class PluginImage{
    protected $loadedData;
    protected $customerFactory;


    public function __construct(CustomerFactory $customerFactory){
        $this->customerFactory=$customerFactory;
    }

    public function afterGetData(\Magento\Customer\Model\Customer\DataProvider $subject,$result){
        foreach ($result as $key=>$value){
            $customer_data=$value;
        }
        $customerId=isset($customer_data['customer']['entity_id'])?$customer_data['customer']['entity_id']:null;
        $customerObj = $this->customerFactory->create()->load($customerId);
        $customerData=$customerObj->getData();
        if(isset($customerData['avatar'])){
            unset($result[$customerId]['customer']['avatar']);
            $result[$customerId]['customer']['avatar'][0]['name']=$customerData['avatar'];
            $result[$customerId]['customer']['avatar'][0]['url']=$customerObj->getCertificateUrl();
        }
        return $result;
    }
}
