<?php
namespace Magenest\Staff\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class Type extends Column{
    public function __construct(ContextInterface $context, UiComponentFactory $uiComponentFactory, array $components = [], array $data = [])
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        $a=$dataSource;
        foreach ($dataSource['data']['items'] as & $item){
            switch ($item['type']){
                case 1:
                    $item['type']="lv1";
                    break;
                case 2:
                    $item['type']="lv2";
                    break;
                default:
                    $item['type']="other";
            }
        }
        return $dataSource;
    }
}
