<?php
namespace Magenest\Staff\Block\Staff;

use Magento\Framework\View\Element\Template;
use Magento\Customer\Model\SessionFactory;
use Magenest\Staff\Model\StaffFactory;

class Update extends Template{

    private $sessionCustomerFactory;
    private $staffFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        SessionFactory $sessionCustomerFactory,
        StaffFactory $staffFactory
    )
    {
        $this->staffFactory=$staffFactory;
        $this->sessionCustomerFactory=$sessionCustomerFactory;
        parent::__construct($context, $data);
    }

    public function getUpdateUrl(){
        return $this->getUrl("staff/customer/beastaff");
    }

    public function getCustomerDetail(){
        $customer = $this->sessionCustomerFactory->create();
        $idCustomer = $customer->getCustomer()->getId();

        $customerData = $this->staffFactory->create()->load($idCustomer,"customer_id");

        return $customerData->getData();

    }

    public function getCustomerId(){
        $customer = $this->sessionCustomerFactory->create();
        $idCustomer = $customer->getCustomer()->getId();
        return $idCustomer;
    }
}
