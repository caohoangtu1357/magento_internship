<?php

namespace Magenest\RestAPI\Api\Data;

/**
 *
 * @api
 */

interface ActorInterface{

    /**
     * @param int $id
     * @return string[]
     **/
    public function get($id);

    /**
     *
     * @return int
     **/
    public function getId();

    /**
     *
     * @return string
     **/
    public function getName();

    /**
     *
     * @param int $id
     * @return $this
     **/
    public function setId($id);

    /**
     *
     * @param string $name
     * @return $this
     **/
    public function setName($name);

}
