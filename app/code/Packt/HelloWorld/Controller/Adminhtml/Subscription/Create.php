<?php
namespace Packt\HelloWorld\Controller\Adminhtml\Subscription;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Packt\HelloWorld\Model\SubscriptionFactory;

class Create extends \Magento\Backend\App\Action{

    private $subscriptionFactory;
    public function __construct(Action\Context $context,SubscriptionFactory $subscriptionFactory)
    {
        $this->subscriptionFactory = $subscriptionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $post=$this->getRequest()->getPostValue();
        $subscription = $this->subscriptionFactory->create();
        $subscription->setFirstname($post["firstname"]);
        $subscription->setLastname($post["lastname"]);
        $subscription->setMessage($post["message"]);
        $subscription->setEmail($post["email"]);
        $subscription->save();

        $this->messageManager->addSuccess(__('Create success'));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    public function _isAllowed()
    {
        return true;
    }
}
