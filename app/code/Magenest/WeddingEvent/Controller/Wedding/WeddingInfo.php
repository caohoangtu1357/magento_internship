<?php

namespace Magenest\WeddingEvent\Controller\Wedding;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magenest\WeddingEvent\Model\ResourceModel\WeddingEvent\CollectionFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class WeddingInfo extends Action{

    private $collectionFactory;
    private $jsonFactory;

    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        JsonFactory $jsonFactory
    )
    {
        $this->jsonFactory=$jsonFactory;
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $post=$this->getRequest()->getPostValue();

        $collection = $this->collectionFactory->create();
        $weddingInfo = $collection->addFieldToSelect("*")
            ->addFieldToFilter("groom_email", $post['groom_email'])
            ->addFieldToFilter("bride_email",$post['bride_email'])
            ->getData();

        $json = $this->jsonFactory->create();

        if(count($weddingInfo)>0){
            $json->setData($weddingInfo[0]);
        }else{
            $json->setData([]);
        }
        return $json;
    }
}
