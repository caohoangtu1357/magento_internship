<?php
namespace Magenest\Test\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel;

class TestAddress extends AbstractModel{
    protected $_eventPrefix = 'test_address';

    public function __construct(Context $context, \Magento\Framework\Registry $registry, ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = [])
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function _construct()
    {
        $this->_init("Magenest\Test\Model\ResourceModel\TestAddress");
    }
}
