<?php

namespace Magenest\HoangTu\Controller\Adminhtml\Hotel;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magenest\HoangTu\Model\HotelFactory;
use Magento\Framework\Controller\ResultFactory;


class CreatePost extends Action{

    private $hotelFactory;
    protected $resultFactory;

    public function __construct(
        Action\Context $context,
        HotelFactory $hotelFactory,
        ResultFactory $resultFactory
    )
    {
        $this->hotelFactory=$hotelFactory;
        $this->resultFactory=$resultFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();

        $hotel=$this->hotelFactory->create();

        if(isset($post["hotel_id"])){
            $hotel->load($post["hotel_id"]);
        }

        $hotel->setHotelName($post['hotel_name']);
        $hotel->setLocationStreet($post['location_street']);
        $hotel->setLocationCity($post['location_city']);
        $hotel->setLocationState($post['location_state']);
        $hotel->setContactPhone($post['contact_phone']);
        $hotel->setTotalAvailableRoom($post['total_available_room']);
        $hotel->setAvailableSingle($post['available_single']);
        $hotel->setAvailableTriple($post['available_triple']);
        $hotel->setAvailableDouble($post['available_double']);
        $hotel->save();

        $this->messageManager->addSuccess("Post Successfully");
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $redirect->setPath("*/*/");
    }
}
