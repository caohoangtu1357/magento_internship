define(['jquery'],function($,alert){
    'use strict';

    return function observerOrderId(){

        $("#order").change(function(){
            $.get("getquestion/id/"+$("#order").val(), function (res) {
                var options = [];
                $("#question").empty();
                for(let i=0; i<res.length;i++){
                    options.push(`<option value="${res[i].entity_id}">${res[i].question}</option>`);
                }
                $("#question").append(options);
            });
        });

    }
})
