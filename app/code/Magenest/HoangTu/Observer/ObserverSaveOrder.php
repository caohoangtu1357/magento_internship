<?php
namespace Magenest\HoangTu\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\OrderFactory;
use Magenest\HoangTu\Model\HotelFactory;
use Magento\Framework\Serialize\Serializer\Json;


class ObserverSaveOrder implements ObserverInterface{

    protected $orderFactory;
    protected $hotelFactory;
    protected $json;

    public function __construct(
        OrderFactory $orderFactory,
        HotelFactory $hotelFactory,
        Json $json
    ){
        $this->json=$json;
        $this->hotelFactory=$hotelFactory;
        $this->orderFactory=$orderFactory;
    }

    public function decreaseHotelQty($idHotel,$roomType){
        $hotelObj =$this->hotelFactory->create();
        $hotelObj=$hotelObj->load($idHotel);
        $hotel = $hotelObj->getData();
        $qty = $hotel["available_$roomType"];
        switch ($roomType){
            case "single":
                $hotelObj->setAvailableSingle($qty-1);
                break;
            case "double":
                $hotelObj->setAvailableDouble($qty-1);
                break;
            case "triple":
                $hotelObj->setAvailableTriple($qty-1);
                break;
        }
        $hotelObj->save();
    }


    public function execute(Observer $observer)
    {
        $order=$this->orderFactory->create();

        $items = $order->load($observer->getData("order_ids")[0])->getAllVisibleItems();
        foreach ($items as $item){
            $itemData = $item->getOrigData("product_options");
            $data = $this->json->unserialize($itemData);
            $options = $data["info_buyRequest"];
            $this->decreaseHotelQty($options["hotel_id"],$options["roomtype"]);
        }
        return $observer;
    }
}
