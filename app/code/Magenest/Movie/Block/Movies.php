<?php
namespace Magenest\Movie\Block;

class Movies extends \Magento\Framework\View\Element\Template{

    protected $collectionFactory;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magenest\Movie\Model\ResourceModel\Movie\CollectionFactory $collectionFactory,
        array $data = []
    ){
        parent::__construct($context,$data);
        $this->collectionFactory=$collectionFactory;
    }

    public function getMovies(){
        $movieCollection = $this->collectionFactory->create();
        $movies=$movieCollection->getMovies();
        return $movies;
    }
}
