<?php

namespace Magenest\SalesAgent\Block\Adminhtml\SalesAgent;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class Report extends Template{

    private $collectionFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context, $data);
    }

    public function getProductWithSalesAgent(){
        $productCollection = $this->collectionFactory->create();
        $products = $productCollection->addAttributeToFilter("sale_agent_id",["neq"=>"NULL"])->getData();
        return $products;
    }

    public function getReportDataUrl(){
        return $this->getUrl("salesagent/salesagent/getreport");
    }

}
