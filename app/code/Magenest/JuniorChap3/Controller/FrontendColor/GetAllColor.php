<?php
namespace Magenest\JuniorChap3\Controller\FrontendColor;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magenest\JuniorChap3\Helper\Color;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Controller\Result\JsonFactory;

class GetAllColor extends Action{

    private $color;
    private $json;
    private $jsonFactory;

    public function __construct(
        Context $context,
        Color $color,
        Json $json,
        JsonFactory $jsonFactory
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->json = $json;
        $this->color = $color;
        parent::__construct($context);
    }

    public function execute()
    {
        $colorsJson = $this->color->getConfigColor("color_option");
        $colors = $this->json->unserialize($colorsJson);
        return $this->jsonFactory->create()->setData($colors);
    }
}
