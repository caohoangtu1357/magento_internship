<?php
namespace Magenest\Movie\Block\Adminhtml\Config\Frontend;

use Magento\Framework\Data\Form\Element\AbstractElement;

class RowInActor extends \Magento\Config\Block\System\Config\Form\Field
{
    protected function _getElementHtml(AbstractElement $element)
    {
        $element->setReadonly(true);
        return parent::_getElementHtml($element);
    }
}
