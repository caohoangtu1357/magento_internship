<?php

namespace Magenest\Test\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class TestAddress extends AbstractDb{

    protected function _construct()
    {
        $this->_init("magenest_test_address","id");
    }

    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        return parent::load($object, $value, $field);
    }
}
