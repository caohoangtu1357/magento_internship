<?php
namespace Magenest\Movie\Controller\Adminhtml\Movie;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magenest\Movie\Model\MovieFactory;

class PostDelete extends \Magento\Backend\App\Action{

    private $movieFactory;
    public function __construct(Action\Context $context,MovieFactory $movieFactory)
    {
        $this->movieFactory = $movieFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $request=$this->getRequest();
        $movie_id=$request->getParam("movie_id");
        $movie = $this->movieFactory->create();
        $a=$movie->load($movie_id);
        $a->delete($movie_id);
        $this->messageManager->addSuccess(__('Delete success'));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed("Magenest_Movie::config_magenest_movie");
    }
}
