<?php

namespace Magenest\SalesAgent\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;

class SalesAgentOption extends AbstractSource
{


    protected $_options;
    private $collectionFactory;

    /**
     * getAllOptions
     *
     * @return array
     */

    public function __construct(
        CollectionFactory $collectionFactory
    ){
        $this->collectionFactory=$collectionFactory;
    }

    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [];

            $collection = $this->collectionFactory->create();

            $customerAgents = $collection->addFieldToSelect("*")
                ->addAttributeToFilter("is_sales_agent",1)
                ->getData();

            foreach ($customerAgents as $customerAgent){
                $this->_options[]=["label"=>$customerAgent["firstname"]." ".$customerAgent["lastname"],"value"=>$customerAgent['entity_id']];
            }
        }

        return $this->_options;
    }

    final public function toOptionArray()
    {
        $options = $this->getAllOptions();

        foreach ($options as $option){
            $options[]=["label"=>$option["label"],"value"=>$option["value"]];
        }

        return $options;
    }
}
