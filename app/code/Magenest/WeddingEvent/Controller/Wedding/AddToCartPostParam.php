<?php
namespace Magenest\WeddingEvent\Controller\Wedding;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Block\Product\ListProduct;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class AddToCartPostParam extends Action{

    private $productFactory;
    private $listProduct;
    private $jsonFactory;
    private $productCollectionFactory;

    public function __construct(
        Context $context,
        ProductFactory $productFactory,
        ListProduct $listProduct,
        JsonFactory $jsonFactory,
        CollectionFactory $productCollectionFactory
    )
    {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->jsonFactory=$jsonFactory;
        $this->listProduct =$listProduct;
        $this->productFactory=$productFactory;
        parent::__construct($context);
    }

    public function getProductPostParam(\Magento\Catalog\Model\Product $product){
        return $this->listProduct->getAddToCartPostParams($product);
    }

    public function execute()
    {
        $weddingId = $this->getRequest()->getParam("id");

        $productObj = $this->productCollectionFactory->create();
        $products = $productObj->addFieldToSelect("*")
                            ->addAttributeToFilter("wedding_id",$weddingId)
                            ->getData();

        $product = $this->productFactory->create()->load($products[0]["entity_id"]);
        $postParam = $this->getProductPostParam($product);
        $json = $this->jsonFactory->create();
        $json->setData($postParam);
        return $json;
    }
}
