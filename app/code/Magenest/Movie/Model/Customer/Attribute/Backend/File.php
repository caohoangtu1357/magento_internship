<?php
namespace Magenest\Movie\Model\Customer\Attribute\Backend;
use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Request\Http;
use Magento\Framework\FileSystem;
use Magento\Framework\ObjectManagerInterface;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Psr\Log\LoggerInterface;

class File extends AbstractBackend{
    const ADDITIONAL_DATA_PREFIX="_additional_data_";

    protected $_filesystem;
    protected $_fileUploaderFactory;
    private $imageUploader;
    protected $_logger;
    private $objectManager;
    protected $request;

    public function __construct(
        LoggerInterface $logger,
        Http $request,
        FileSystem $fileSystem,
        UploaderFactory $uploaderFactory,
        ObjectManagerInterface $objectManager
    ){
        $this->_filesystem=$fileSystem;
        $this->_fileUploaderFactory=$uploaderFactory;
        $this->_logger=$logger;
        $this->request=$request;
        $this->objectManager=$objectManager;
    }

    private function getImageUploader(){
        if($this->imageUploader===null){
            $this->imageUploader=$this->objectManager->get(\Magento\Catalog\CategoryImageUpload::class);
        }
        return $this->imageUploader;
    }

    protected function getUploadedImageName($value){
        if(is_array($value) && isset($value[0]['name'])){
            return '/'.$value[0]['name'];
        }
        return '';
    }

    public function beforeSave($object)
    {
//        $b=$this->request;
//        $customer_params=$this->request->getParam('customer');
//        $attributeName=$this->getAttribute()->getName();
//        $value=array();
//        if(isset($customer_params['image'])){
//            $value=$customer_params['image'];
//            if($imageName=$this->getUploadedImageName($value)){
//                $object->setData($attributeName,$value);
//                $object->setData($attributeName,$imageName);
//            }
//        }elseif (count($customer_params)>0){
//            $object->setData($attributeName,null);
//        }
        return parent::beforeSave($object); // TODO: Change the autogenerated stub
    }

}
