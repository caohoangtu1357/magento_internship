<?php
namespace Magenest\Movie\Model\Config\Source;

use Magenest\Movie\Model\ResourceModel\Director\CollectionFactory;

class DirectorOption implements \Magento\Framework\Option\ArrayInterface{

    protected $collectionFactory;

    public function __construct(CollectionFactory $collectionFactory){
        $this->collectionFactory=$collectionFactory;
    }

    public function toOptionArray()
    {

        $collection = $this->collectionFactory->create();
        $directors= $collection->getDirectors();
        $options=[];
        foreach ($directors as $director){
            $options[]=["label"=>$director["name"],"value"=>$director["director_id"]];
        }

        return $options;
    }
}
