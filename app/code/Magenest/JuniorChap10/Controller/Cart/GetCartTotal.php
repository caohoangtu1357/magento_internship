<?php
namespace Magenest\JuniorChap10\Controller\Cart;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Checkout\Model\CartFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class GetCartTotal extends Action{

    private $cartFactory;
    private $jsonFactory;

    public function __construct(
        Context $context,
        CartFactory $cartFactory,
        JsonFactory $jsonFactory
    )
    {
        $this->jsonFactory = $jsonFactory;
        $this->cartFactory = $cartFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $cart = $this->cartFactory->create();
        $total = $cart->getQuote()->getGrandTotal();
        $json = $this->jsonFactory->create();
        return $json->setData(["grand-total"=>$total]);
    }
}
