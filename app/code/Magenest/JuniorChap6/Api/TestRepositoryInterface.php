<?php

namespace Magenest\JuniorChap6\Api;
use \Magenest\JuniorChap6\Api\Data\TestInterface;

interface TestRepositoryInterface{

    /**
     * @param \Magenest\JuniorChap6\Api\Data\TestInterface $test
     * @return \Magenest\JuniorChap6\Api\Data\TestInterface
     */
    public function save(TestInterface $test);

    /**
     * @param int $id
     * @return \Magenest\JuniorChap6\Api\Data\TestInterface
     */
    public function getById($id);

    /**
     * @param int $id
     * @return void
     */
    public function delete($id);
}
