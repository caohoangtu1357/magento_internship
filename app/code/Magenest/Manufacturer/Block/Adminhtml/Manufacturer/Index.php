<?php


namespace Magenest\Manufacturer\Block\Adminhtml\Manufacturer;

use Magenest\Manufacturer\Model\ManufacturerFactory;
use Magento\Backend\Block\Template;
use Magenest\Manufacturer\Model\ResourceModel\Manufacturer\CollectionFactory;

class Index extends Template
{
    private $fullModuleList;
    private $manufacturerFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        CollectionFactory $manufacturerFactory
    )
    {
        $this->manufacturerFactory=$manufacturerFactory;
        parent::__construct($context, $data);
    }

    public function getManufacturers()
    {
        $manufacturer= $this->manufacturerFactory->create();
        $manufacturer->addFieldToSelect('*');
        $manufactures=$manufacturer->getData();
        return $manufactures;
    }


    public function getSubmitUrl()
    {
        return $this->getUrl("manufacturer/manufacturer/postcreate");
    }
}
