<?php
namespace Magenest\Movie\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

class ChangeTextFieldInSystemObserver implements ObserverInterface{

    protected $request;
    protected $configWriter;

    public function __construct(
        RequestInterface $request,
        WriterInterface $configWriter
    ) {
        $this->request = $request;
        $this->configWriter = $configWriter;
    }

    public function execute(Observer $observer)
    {
        $meetParams = $this->request->getParam('groups');
        $text = $meetParams['moviepage']['fields']['text_field']['value'];
        if($text=="Ping"){
            $this->configWriter->save('movie/moviepage/text_field', "Pong");
        }
        return $this;
    }
}
