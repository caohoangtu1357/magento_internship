<?php

namespace Magenest\CountDown\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class CountDown extends AbstractDb{


    protected function _construct()
    {
        $this->_init("magenest_countdown","id");
    }

    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        return parent::load($object, $value, $field);
    }
}
