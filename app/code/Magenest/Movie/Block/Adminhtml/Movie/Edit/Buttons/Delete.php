<?php
namespace Magenest\Movie\Block\Adminhtml\Movie\Edit\Buttons;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Layout\Generic;

class Delete extends Generic implements ButtonProviderInterface
{
    protected $url;
    protected $request;
    public function __construct(
        UiComponentFactory $uiComponentFactory,
        $data = [],
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\App\Request\Http $request    )
    {
        $this->request=$request;
        $this->url=$url;
        parent::__construct($uiComponentFactory, $data);
    }

    public function getButtonData()
    {
        $data = [];

        if ($this->request->getParam('id')) {
            $data = [
                'label' => __('Delete Movie'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm(\'' . __(
                        'Are you sure you want to do this?'
                    ) . '\', \'' . $this->getDeleteUrl() . '\')',
                'sort_order' => 20,
            ];
        }
        return $data;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->url->getUrl('*/*/postdelete', ['movie_id' => $this->request->getParam('id')]);
    }
}
