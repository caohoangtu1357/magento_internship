<?php
namespace Magenest\Staff\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magenest\Staff\Model\StaffFactory;
use Magento\Customer\Model\SessionFactory;

class AppendLevelToPrice implements ObserverInterface{

    private $staffFactory;
    private $sessionFactory;

    public function __construct(
        StaffFactory $staffFactory,
        SessionFactory $sessionFactory
    ){
        $this->sessionFactory = $sessionFactory;
        $this->staffFactory=$staffFactory;
    }

    public function execute(Observer $observer)
    {
//        $customerId = $this->sessionFactory->create()->getCustomer()->getId();
//
//        $staff = $this->staffFactory->create();
//        $staffData = $staff->load($customerId,"customer_id")->getData();
//        if(count($staffData)>0){
//            $level="";
//            switch ($staffData["type"]){
//                case 1:
//                    $level= "(lv1)";
//                    break;
//                case 2:
//                    $level= "(lv2)";
//                    break;
//                default:
//                    $level="other";
//            }
//            $price = $observer->getProduct()->getPrice();
//            $observer->getEvent()->getProduct()->setPrice(0);
//
//        }

        return $observer;
    }
}
