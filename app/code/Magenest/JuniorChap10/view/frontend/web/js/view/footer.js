define(
    [
        'ko',
        'uiComponent',
        'underscore',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/totals',
        'jquery'
    ],
    function (
        ko,
        Component,
        _,
        stepNavigator,
        customer,
        quote,
        totals,
        $
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Magenest_JuniorChap10/footer'
            },



            //add here your logic to display step,
            isVisible: ko.observable(true),
            grandTotal:ko.observable(0),
            isLogedIn: customer.isLoggedIn(),
            //step code will be used as step content id in the component template
            stepCode: 'login',
            //step title value
            stepTitle: 'Login',
            email:"asdasd",
            allStep:[],

            /**
             *
             * @returns {*}
             */
            initialize: function () {
                this._super();
                this.checkIsPayment();


                var self = this;

                setInterval(function(){
                    self.getGrandTotal();
                    if(self.getCurrentStep()==="payment"){
                        $(".action.primary.checkout").css("display","none");
                        $("#btn-continue").text("Pay Now");

                    }else{
                        // console.log(totals);
                        $("#btn-continue").text("Continue");
                        $(".fieldset.hidden-fields").css("display","none");

                    }
                },1500);


                return this;
            },

            /**
             * The navigate() method is responsible for navigation between checkout step
             * during checkout. You can add custom logic, for example some conditions
             * for switching to your custom step
             */
            navigate: function () {

                this.isVisible(true);
            },

            getGrandTotal:function(){
                var self = this;
                $.ajax({
                    type: 'GET',
                    url: '../junior10/cart/getcarttotal',
                    context: this,
                    success: function(res) {
                        var total = res["grand-total"];
                        self.grandTotal(total);
                    }
                });
            },


            /**
             * @returns void
             */
            navigateToNextStep: function () {
                $.ajax({
                    type: 'POST',
                    url: '../customer/ajax/login',
                    context: this,
                    data:
                        JSON.stringify({
                            username:$("#username").val(),
                            password:$("#password").val(),
                            context:"checkout",
                            form_key:$("input[name=form_key]").val()
                        })
                    ,
                    success: function(res) {
                        if(res.errors==false){
                            window.location.replace(window.location.href.replace("#login","#shipping"));
                            window.location.reload();
                        }else{
                            var data = JSON.parse(localStorage['mage-cache-storage']);
                            data["checkout-data"].inputFieldEmailValue=$("#username").val();
                            data["checkout-data"].validatedEmailValue="";
                            data["checkout-data"].inputFieldEmailValue=$("#username").val();
                            localStorage['mage-cache-storage']=JSON.stringify(data);
                            window.location.replace(window.location.href.replace("#login","#shipping"));
                            window.location.reload();
                        }

                    }
                });

            },
            getCurrentStep:function(){
                return window.location.href.split("#")[1];
            },

            prev: function () {
                debugger;

                var activeIndex = 0,
                    code;

                stepNavigator.steps().sort(stepNavigator.sortItems).forEach(function (element, index) {
                    if (element.isVisible()) {
                        element.isVisible(false);
                        activeIndex = index;
                    }
                });

                if(activeIndex==0){
                    window.location.replace("../checkout/cart/");
                    return;
                }

                $("#btn-continue").text("Continue");

                if (stepNavigator.steps().length > 0) {
                    code = stepNavigator.steps()[activeIndex -1].code;
                    stepNavigator.steps()[activeIndex - 1].isVisible(true);
                    stepNavigator.setHash(code);
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            },
            next:function(){
                debugger;

                if(this.getCurrentStep()=="shipping"){
                    $(".button.action.continue.primary").click();
                    this.checkIsPayment();
                    // stepNavigator.next();
                }
                if(this.getCurrentStep()=="login"){
                    $("#cadotahen").click();
                    this.checkIsPayment();
                }
                if(this.getCurrentStep()=="payment"){
                   $(".action.primary.checkout").click();
                    this.checkIsPayment();
                    $("#btn-continue").text("Pay Now");
                }
            },

            checkIsPayment:function(){

                if(this.getCurrentStep()=="payment"){
                    setTimeout(function(){
                        setInterval(function(){
                            $(".action.primary.checkout").css("display","none");
                            }, 1000);
                        // $(".action.primary.checkout").css("display","none");
                        $("#btn-continue").text("Pay Now");
                    },2000);

                }else{
                    setTimeout(function(){
                        $("#btn-continue").text("Continue");
                    },2000);
                }
            }



        });
    }
);
