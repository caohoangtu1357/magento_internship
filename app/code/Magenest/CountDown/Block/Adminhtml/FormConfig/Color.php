<?php


namespace Magenest\CountDown\Block\Adminhtml\FormConfig;

class Color extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @var \Magento\Framework\Data\Form\Element\Text
     */
    public $text;

    /**
     * Colorpicker constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Data\Form\Element\Text $text
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Data\Form\Element\Text $text
    )
    {
        $this->text = $text;
        parent::__construct($context);
    }

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     *
     * @return string
     */
    public function _getElementHtml(
        \Magento\Framework\Data\Form\Element\AbstractElement $element
    )
    {
        // Use Varien text element as a basis
        $input = $this->text;

        // Set data from config element on Varien text element
        $input->setForm($element->getForm())
            ->setElement($element)
            ->setValue($element->getValue())
            ->setHtmlId($element->getHtmlId())
            ->setClass('ddg-colpicker')
            ->setName($element->getName());

        // Inject updated Varien text element HTML in our current HTML
        $html = $input->getHtml();


        $html.='<script type="text/javascript">
            require(["jquery"], function ($) {
                    var $el = $("#'.$element->getHtmlId().'");

                    $el.css("backgroundColor", "");


                    $(document).click(function(){
                        $el.css("backgroundColor",$el.val());
                    });
                    $(document).ready(function(){
                        $el.css("backgroundColor",$el.val());
                    })
            });
            </script>';
        return $html;
    }
}

