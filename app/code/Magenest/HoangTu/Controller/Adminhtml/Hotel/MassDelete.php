<?php

namespace Magenest\HoangTu\Controller\Adminhtml\Hotel;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magenest\HoangTu\Model\HotelFactory;
use Magento\Ui\Component\MassAction\Filter;


class MassDelete extends Action{

    private $hotelFactory;
    private $filter;
    private $collectionFactory;

    public function __construct(
        Action\Context $context,
        HotelFactory $hotelFactory,
        Filter $filter,
        \Magenest\HoangTu\Model\ResourceModel\Hotel\CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory=$collectionFactory;
        $this->filter=$filter;
        $this->hotelFactory=$hotelFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $collections = $this->filter->getCollection($this->collectionFactory->create())->load();
        $a=$collections->getSize();

        foreach ($collections as $collection){
            $collection->delete();
        }

        $this->messageManager->addSuccess("Total of $a deleted");

        $redirect = $this->resultFactory->create("redirect");
        return $redirect->setPath("*/*/");

    }
}
