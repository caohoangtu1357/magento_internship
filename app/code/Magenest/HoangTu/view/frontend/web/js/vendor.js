define(['jquery','Magento_Ui/js/modal/alert'],function($,alert){
    return function handlerButtonGetInfo(){

        setTimeout(function(){
            $("#submit_button").click(function(){
                var idCustomer = $("#id_customer").val();
                var urlSubmit = $("#submit_url").val()+"id/"+idCustomer;

                $.get(urlSubmit,function(res){
                    var message="";
                    if(res.status=="notFound"){
                        message="Customer not found";
                    }else{
                        message = `
                        ID: ${res.entity_id} <br>
                        Name: ${res.firstname+" "+res.lastname}<br>
                        Email: ${res.email}
                        `;
                    }
                    alert({
                        title: 'Customer Information',
                        content: message
                    });
                });
            });
        },3000);

    }
})
