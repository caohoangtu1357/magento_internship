<?php
namespace Magenest\SalesAgent\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;

class SalesAgent extends AbstractModel{
    public function __construct(
        Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function _construct()
    {
        $this->_init("Magenest\SalesAgent\Model\ResourceModel\SalesAgent");
    }
}


