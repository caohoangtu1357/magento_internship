<?php

namespace Magenest\JuniorChap5\Model\Config\Source;

class CustomerGroup extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    private $customerGroupCollection;

    public function __construct(
        \Magento\Customer\Model\ResourceModel\Group\Collection $customerGroupCollection
    ){
        $this->customerGroupCollection=$customerGroupCollection;
    }

    public function getCustomerGroupOptions() : array{
        $customerGroups = $this->customerGroupCollection->getData();
        $options = [];
        foreach ($customerGroups as $customerGroup){
            $options[] = ['label' => $customerGroup['customer_group_code'], 'value' => $customerGroup['customer_group_id']];
        }
        return $options;
    }

    public function getAllOptions()
    {
        $options  = $this->getCustomerGroupOptions();
        return $options;
    }
}

