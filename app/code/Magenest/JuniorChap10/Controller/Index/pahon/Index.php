<?php
namespace Magenest\JuniorChap10\Controller\Index\pahon;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\View\Result\Page;

class Index extends \Magento\Framework\App\Action\Action{

    private $pageFactory;
    private $productFactory;
    private $collectionFactory;


    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Product $productFactory,
        CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->productFactory = $productFactory;
        $this->pageFactory = $pageFactory;
        parent::__construct($context);


    }

    public function execute()
    {
        $this->productFactory->load(49)->setName("xcv");

        $page = $this->pageFactory->create();

        $q=$this->productFactory->load(49);


        $collection = $this->collectionFactory->create();

        $pahon = $collection
            ->addAttributeToFilter("entity_id",50)

//            ->addAttributeToSelect("type_varchar")
            ->setPageSize(100)->getItems();

        return $page;
    }
}
