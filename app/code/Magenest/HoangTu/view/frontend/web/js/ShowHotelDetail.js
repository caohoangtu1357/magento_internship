define(['jquery','Magento_Ui/js/modal/alert'],function($,alert){
    return function handlerButtonGetInfo(){

        setTimeout(function(){
            $("#hotel_location").change(function(){
                var idHotel = $("#hotel_location").val();
                var detailUrl = $("#ajax_detail_hotel").val()+"id/"+idHotel;

                $.get(detailUrl,function(res){
                    var message="";

                    message = `
                    ID: ${res.hotel_id} <br>
                    Name: ${res.hotel_name}<br>
                    Location: ${res.location_street+" "+res.location_city+" "+res.location_state}<br>
                    Contact phone: ${res.contact_phone}<br>
                    Name: ${res.hotel_name}<br>
                    Room quantity: single: ${res.available_single}, double ${res.available_double}, triple ${res.available_triple}
                    `;

                    alert({
                        title: 'Hotel Detail',
                        content: message
                    });
                });


            });
        },2000);

    }
})
