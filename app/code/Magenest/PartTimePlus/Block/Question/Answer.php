<?php

namespace Magenest\PartTimePlus\Block\Question;

use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Customer\Model\SessionFactory;


class Answer extends Template{

    private $collectionFactory;
    private $sessionFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        CollectionFactory $collectionFactory,
        SessionFactory $sessionFactory
    )
    {
        $this->sessionFactory=$sessionFactory;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    public function getAllOrder($idCustomer){
        $collection = $this->collectionFactory->create();
        return $collection->addFieldToSelect("*")
            ->addFieldToFilter("customer_id",$idCustomer)->getItems();

    }

    public function getOrderIdContainQuestion(){
        $idCustomer = $this->sessionFactory->create()->getCustomer()->getId();
        $orders = $this->getAllOrder($idCustomer);

        $orderIdHaveQuestion = [];
        foreach ($orders as $order){
            foreach ($order->getAllItems() as $item){
                if($item->getProduct()->getQuestion()){
                    array_push($orderIdHaveQuestion,$order->getEntityId());
                }
            }
        }
        return $orderIdHaveQuestion;
    }
}
