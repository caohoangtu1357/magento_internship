<?php
namespace Magenest\Manufacturer\Block\Manufacturerfrontend;
use Magento\Framework\View\Element\Template;

class ManufactureName extends \Magento\Framework\View\Element\Template{
    private $request;
    private $productFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        \Magento\Catalog\Model\ProductFactory $productFactory
    )
    {
        $this->productFactory=$productFactory;
        parent::__construct($context, $data);
    }

    public function getManufacturerId(){
        $id = $this->getRequest()->getParam('id');
        $product = $this->productFactory->create()->load($id);
        $id=$product->getOrigData('manufacturer_id');
        return $id;
    }

    public function getManufacturerName(){
        $id = $this->getRequest()->getParam('id');
        $product = $this->productFactory->create()->load($id);
        $name=$product->getAttributeText('manufacturer_id');
        return $name;
    }

    public function getUrlGetManufacurer(){
        return $this->getUrl("manufacturer/manufacturer/getmanufacturer");
    }

}
