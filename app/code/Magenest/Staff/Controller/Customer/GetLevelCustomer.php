<?php
namespace Magenest\Staff\Controller\Customer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magenest\Staff\Model\StaffFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Customer\Model\SessionFactory;

class GetLevelCustomer extends Action{

    private $staffFactory;
    private $jsonFactory;
    private $sessionFactory;

    public function __construct(
        Context $context,
        StaffFactory $staffFactory,
        JsonFactory $jsonFactory,
        SessionFactory $sessionFactory
    )
    {
        $this->sessionFactory=$sessionFactory;
        $this->jsonFactory=$jsonFactory;
        $this->staffFactory=$staffFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $idCustomer = $this->sessionFactory->create()->getCustomer()->getId();
        $json = $this->jsonFactory->create();

        if($idCustomer){
            $staff = $this->staffFactory->create();
            $staffData = $staff->load($idCustomer,"customer_id")->getData();
            $json->setData(["status"=>"found","staff"=>$staffData]);
        }else{
            $json->setData(["status"=>"not_found"]);
        }
        return $json;
    }
}
