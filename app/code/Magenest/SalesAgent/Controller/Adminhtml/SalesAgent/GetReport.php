<?php
namespace Magenest\SalesAgent\Controller\Adminhtml\SalesAgent;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magenest\SalesAgent\Model\ResourceModel\SalesAgent\CollectionFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class GetReport extends Action{

    private $collectionFactory;
    private $jsonFactory;

    public function __construct(
        Action\Context $context,
        CollectionFactory $collectionFactory,
        JsonFactory $jsonFactory
    )
    {
        $this->jsonFactory=$jsonFactory;
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $sku = $this->getRequest()->getPostValue()['sku'];
        $reports = $this->collectionFactory->create()
            ->addFieldToFilter("order_item_sku",$sku)
            ->addFieldToSelect("*")
            ->getData();
        $json = $this->jsonFactory->create();
        $json->setData($reports);
        return $json;
    }
}
