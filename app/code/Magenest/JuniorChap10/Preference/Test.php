<?php
namespace Magenest\JuniorChap10\Preference;

use Magento\Framework\View\Element\Template;
use Magenest\JuniorChap10\Block\Test\TestFactory;

class Test extends \Magento\Framework\View\Element\Template{
    public function __construct(
        Template\Context $context,
        array $data = [],
        TestFactory $testFactory
    )
    {
        parent::__construct($context, $data);
    }
}
