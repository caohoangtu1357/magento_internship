<?php
namespace Magenest\Cybergame\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel;

class RoomExtraOption extends AbstractModel{
    public function __construct(Context $context, \Magento\Framework\Registry $registry, ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = [])
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function _construct()
    {
        $this->_init("Magenest\Cybergame\Model\ResourceModel\RoomExtraOption");
    }

}
