<?php
namespace Magenest\PartTimePlus\Controller\Exam;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class GetQuestion extends Action{

    private $collectionFactory;
    private $jsonFactory;

    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        JsonFactory $jsonFactory
    )
    {
        $this->jsonFactory=$jsonFactory;
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context);
    }

    public function getAllOrder($idOrder){
        $customerCollection = $this->collectionFactory->create();
        return $customerCollection->addFieldToSelect("*")
            ->addFieldToFilter("entity_id",$idOrder)->getItems();

    }

    public function getQuestion($idOrder){
        $orders = $this->getAllOrder($idOrder);

        $question = [];
        foreach ($orders as $order){
            foreach ($order->getAllItems() as $item){
                if($item->getProduct()->getQuestion()){
                    array_push($question,[
                        "entity_id"=>$item->getProduct()->getEntityId(),
                        "question"=>$item->getProduct()->getQuestion()
                    ]);
                }
            }
        }
        return $question;
    }

    public function execute()
    {
        $idOrder = $this->getRequest()->getParam("id");
        $questions = $this->getQuestion($idOrder);
        $json = $this->jsonFactory->create()->setData($questions);
        return $json;
    }
}
