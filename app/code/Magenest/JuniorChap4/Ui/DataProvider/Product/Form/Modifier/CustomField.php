<?php

namespace Magenest\JuniorChap4\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Catalog\Model\Locator\LocatorInterface;

class CustomField extends AbstractModifier
{
    private $locator;
    private $coreRegistry;
    private $resource;
    private $stockRegistry;

    protected $meta = [];

    public function __construct(
        LocatorInterface $locator,
        \Magento\Framework\Registry  $coreRegistry,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
    ) {
        $this->locator = $locator;
        $this->_coreRegistry = $coreRegistry;
        $this->_resource = $resource;
        $this->stockRegistry = $stockRegistry;
    }
    /**
     * @param array $meta
     *
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = array_replace_recursive(
            $meta,
            [
                'magenest' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('Custom Fields'),
                                'collapsible' => true,
                                'componentType' => Fieldset::NAME,
                                'dataScope' => 'data.magenest',
                                'sortOrder' => 10
                            ],
                        ],
                    ],
                    'children' => [
                        'status'    => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'label'         => __('Select field'),
                                        'componentType' => Field::NAME,
                                        'formElement'   => Select::NAME,
                                        'dataScope'     => 'option',
                                        'dataType'      => Text::NAME,
                                        'sortOrder'     => 10,
                                        'options'       => [
                                            ['value' => 'option_1', 'label' => __('Option 1')],
                                            ['value' => 'option_2', 'label' => __('Option 2')]
                                        ],
                                    ],
                                ],
                            ],
                        ]
                    ]
                ],
            ]
        );

        return $this->meta;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }
}
