<?php
namespace Magenest\Cybergame\Block\RoomInfo;

use Magento\Framework\View\Element\Template;
use Magenest\Cybergame\Model\ResourceModel\RoomExtraOption\CollectionFactory;
use Magento\Eav\Model\ConfigFactory;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\CustomerFactory;

class EditRoom extends Template{

    private $collectionFactory;
    private $configFactory;
    private $customerSession;
    private $customerFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        CollectionFactory $collectionFactory,
        ConfigFactory $configFactory,
        Session $customerSession,
        CustomerFactory $customerFactory
    )
    {
        $this->customerFactory=$customerFactory;
        $this->customerSession=$customerSession;
        $this->configFactory=$configFactory;
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context, $data);
    }


    public function getPriceAttributeId(){
        $config = $this->configFactory->create();
        $id=$config->getAttribute(\Magento\Catalog\Model\Product::ENTITY, \Magento\Catalog\Api\Data\ProductInterface::PRICE)->getDataByKey("attribute_id");
        return $id;
    }

    public function getNameAttributeId(){
        $config = $this->configFactory->create();
        $id=$config->getAttribute(\Magento\Catalog\Model\Product::ENTITY, \Magento\Catalog\Api\Data\ProductInterface::NAME)->getDataByKey("attribute_id");
        return $id;
    }

    public function getRoomDetail(){

        $attributeId = $this->getPriceAttributeId();
        $nameAttrId=$this->getNameAttributeId();
        $id=$this->getRequest()->getParam('id');

        $rooms=$this->collectionFactory->create()
            ->addFieldToSelect("*")->join(
                "catalog_product_entity_decimal",
                "main_table.product_id = catalog_product_entity_decimal.entity_id and catalog_product_entity_decimal.attribute_id=$attributeId and main_table.id=$id",
                ["price"=>"catalog_product_entity_decimal.value"]
            )->join(
                "catalog_product_entity_varchar",
                "main_table.product_id = catalog_product_entity_varchar.entity_id and catalog_product_entity_varchar.attribute_id=$nameAttrId",
                ["name"=>"catalog_product_entity_varchar.value"]
            )
            ->getData();
        return $rooms[0];
    }

    public function getSubmitUrl(){
        return $this->getUrl("cybergame/roominfo/postedit");
    }

    public function isManager(){
        $idCustomer = $this->customerSession->getCustomer()->getId();
        $customer = $this->customerFactory->create()->load($idCustomer);
        if(isset($customer->getOrigData()['is_manager'])){
            if($customer->getOrigData()['is_manager']==1){
                return true;
            }
        }else{
            return false;
        }
    }

}
