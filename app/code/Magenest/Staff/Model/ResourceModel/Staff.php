<?php
namespace Magenest\Staff\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Staff extends AbstractDb{

    protected function _construct()
    {
        $this->_init("magenest_staff","id");
    }

    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        return parent::load($object, $value, $field);
    }
}
