<?php

namespace Magenest\HoangTu\Block\Vendor;
use Magento\Framework\View\Element\Template;

class Index extends Template{
    function getInfoAjaxUrl(){
        $url =  $this->getUrl("hoang_tu/vendor/customerinfo");
        return $url;
    }
}
