<?php

namespace Magenest\CountDown\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class CountDownData extends  AbstractHelper{

    const XML_PATH_COUNTDOWN = "count_down/";

    private function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    public function getConfigClock($code, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_COUNTDOWN .'count_down_config_page/'. $code, $storeId);
    }
}
