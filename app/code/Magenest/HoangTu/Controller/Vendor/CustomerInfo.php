<?php
namespace Magenest\HoangTu\Controller\Vendor;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Customer\Model\CustomerFactory;

class CustomerInfo extends Action{

    private $jsonFactory;
    private $customerFactory;

    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        CustomerFactory $customerFactory
    )
    {
        $this->customerFactory=$customerFactory;
        $this->jsonFactory=$jsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $idCustomer = $this->getRequest()->getParam("id");
        $customer = $this->customerFactory->create()->load($idCustomer);
        $customerData = $customer->getData();
        if(count($customerData)==0){
            $customerData["status"]="notFound";
        }else{
            $customerData["status"]="found";
        }
        $result = $this->jsonFactory->create();
        $result->setData($customerData);
        return $result;
    }
}
