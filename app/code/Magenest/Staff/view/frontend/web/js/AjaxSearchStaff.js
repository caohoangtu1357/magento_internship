define(['jquery'],function($){
    'use strict';

    var getType=function(type){
      switch (type){
          case 1:
              return "lv1";
          case 2:
              return "lv2";
          default:
              return "other";
      }
    };

    var getStatus=function(status){
      switch (status){
          case 2:
              return "approved";
          default:
              return "pending";
      }
    };

    return function ajaxUpdateUser(){

        $("#key-word").change(function(){
           var url=$("#search-url").val();
           var keyWord = $("#key-word").val();


           $.get(url+"key_word/"+keyWord,function(res){
               var rows=[];
               $("#result").empty();

               if(res.length>0){
                   rows.push(`
                    <tr>
                        <th>nick name</th>
                        <th>type</th>
                        <th>status</th>
                        <th>update_at</th>
                    </tr>`
                   );
               }

               for(let i=0;i<res.length;i++){
                   rows.push(`
                        <tr>
                            <td>${res[i].nick_name}</td>
                            <td>${getType(res[i].type)}</td>
                            <td>${getStatus(res[i].status)}</td>
                            <td>${res[i].update_at}</td>
                        </tr>
                   `);
               }

               $("#result").append(rows);

           });
        });

    }
})
