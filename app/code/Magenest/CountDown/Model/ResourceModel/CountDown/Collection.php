<?php
namespace Magenest\CountDown\Model\ResourceModel\CountDown;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection{
    protected $_idFieldName="id";
    public function _construct()
    {
        $this->_init("Magenest\CountDown\Model\CountDown","Magenest\CountDown\Model\ResourceModel\CountDown");
    }
}
