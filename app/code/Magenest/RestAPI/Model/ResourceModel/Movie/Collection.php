<?php
namespace Magenest\Movie\Model\ResourceModel\Movie;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'movie_id';

    public function _construct()
    {
        $this->_init("Magenest\Movie\Model\Movie",
            "Magenest\Movie\Model\ResourceModel\Movie");
    }

    public function getMovies(){
        $this->addFieldToSelect('*');
        $this->getSelect()
            ->join(
                'magenest_movie_actor',
                'main_table.movie_id = magenest_movie_actor.movie_id'
            )->join(
                'magenest_actor',
                'magenest_movie_actor.actor_id= magenest_actor.actor_id',
                array(
                    'actor_name'  => new \Zend_Db_Expr('group_concat(`magenest_actor`.name SEPARATOR ",")'),
                )
            )->join(
                'magenest_director',
                'main_table.director_id= magenest_director.director_id',
                array(
                    'director_name'=>"magenest_director.name"
                )
            );
        $this->getSelect()->group('main_table.movie_id');
        return $this->getData();
    }

}
