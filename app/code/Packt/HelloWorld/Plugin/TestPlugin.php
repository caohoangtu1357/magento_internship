<?php

namespace Packt\HelloWorld\Plugin;

class TestPlugin
{
    public function beforeSum(\Packt\HelloWorld\Block\Testplugin $subject, $a, $b)
    {
        $a += 1;
        $b += 1;
        return [$a, $b];
    }

    public function afterSum($intercepter, $result)
    {
        $result = $result - $result;
        return $result;
    }

    public function aroundSum($intercepter, callable $proceed)
    {
        return 2;
//        echo __METHOD__ . " - Before proceed() </br>";
//        $result = $proceed();
//        echo __METHOD__ . " - After proceed() </br>";
//
//        return $result;
    }
}
