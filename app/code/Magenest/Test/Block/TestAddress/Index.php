<?php
namespace Magenest\Test\Block\TestAddress;

use Magento\Framework\View\Element\Template;
use Magenest\Test\Model\ResourceModel\TestAddress\CollectionFactory;
class Index extends Template{

    private $collectionFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context, $data);
    }

    public function getAddresses(){
        $addressCollection = $this->collectionFactory->create();
        $addresses = $addressCollection->addFieldToSelect("*")->getData();
        return $addresses;
    }

    public function getEditUrl(){
        return $this->getUrl("*/*/edit");
    }
}
