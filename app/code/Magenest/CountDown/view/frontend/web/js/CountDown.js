define(['jquery', 'uiComponent', 'ko'], function($, Component, ko) {

    var timer =function(item) {
        debugger
        this.time = ko.observable(item.countdown_value);
        return this;
    };


    return Component.extend({

        initialize:function(){
            this._super();

            this.time = ko.observable("Begin timer!");

            this.getTime= ko.pureComputed(function(){
                return this.time();
            },this);
        },


        requestTime:function(){
            var self=this;
            $.ajax({
                type: 'GET',
                url: 'count_down/countdown/gettime',
                context: this,
                success: function(res) {
                    console.log(res);
                    // self.time(res);
                    // self.runCountDown();
                },
                dataType: 'json'
            });
        },

        runCountDown:function(){
            var self = this;

            var now = new Date();

            var now = new Date(now.getFullYear(), now.getMonth(), now.getDay(), 0, 0, 0, 0).getTime();

            var countDownDate = new Date().getTime();

            countDownDate = new Date().getTime();

            var x = setInterval(function() {

                countDownDate=countDownDate-1000;

                var distance =countDownDate-now;

                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                self.time(hours+":"+minutes+":"+seconds);

                if (distance < 0) {
                    self.time("Expired!");
                }
            }, 1000);
        }
    });
});
