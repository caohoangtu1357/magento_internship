<?php
namespace Magenest\HoangTu\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\OrderFactory;
use Magenest\HoangTu\Model\HotelFactory;
use Magento\Framework\Serialize\Serializer\Json;


class ObserverLoadOrder implements ObserverInterface{

    protected $orderFactory;
    protected $hotelFactory;
    protected $json;

    public function __construct(
        OrderFactory $orderFactory,
        HotelFactory $hotelFactory,
        Json $json
    ){
        $this->json=$json;
        $this->hotelFactory=$hotelFactory;
        $this->orderFactory=$orderFactory;
    }


    public function execute(Observer $observer)
    {
        $a=0;
       return $observer;
    }
}
