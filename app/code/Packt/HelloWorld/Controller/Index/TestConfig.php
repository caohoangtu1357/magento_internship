<?php
namespace Packt\HelloWorld\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;

class TestConfig extends \Magento\Framework\App\Action\Action {

    private $configData;

    public function __construct(Context $context,\Packt\HelloWorld\Helper\Data $configData)
    {
        parent::__construct($context);
        $this->configData=$configData;
    }

    public function execute()
    {
        $title = $this->configData->getGeneralConfig("header_title");
        echo $title;
        exit();
    }
}
