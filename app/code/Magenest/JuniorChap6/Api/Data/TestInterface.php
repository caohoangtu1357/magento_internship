<?php
namespace Magenest\JuniorChap6\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface TestInterface extends ExtensibleDataInterface{

    const TEST= 'test';
    const ID= 'id';
    /**
     * @return string
     */
    public function getTest();

    /**
     * @param string $str
     * @return void
     */
    public function setTest($str);
}
