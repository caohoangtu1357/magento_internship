<?php
namespace Magenest\HoangTu\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magenest\HoangTu\Model\VendorFactory;
use Magento\Catalog\Model\ProductFactory;

class ObserverChangeStateOrder implements ObserverInterface{

    private $vendorFactory;
    private $productFactory;

    public function __construct(
        VendorFactory $vendorFactory,
        ProductFactory $productFactory
    ){
        $this->productFactory=$productFactory;
        $this->vendorFactory=$vendorFactory;
    }

    public function execute(Observer $observer)
    {

        $orderStatus = $observer->getData('order')->getData('status');
        if($orderStatus=="processing"){
            $items = $observer->getData('order')->getData('items');
            foreach ($items as $item){
                $idProduct = $item->getData('product_id');
                $qtyShipped = $item->getData('qty_shipped');

                $product = $this->productFactory->create();
                $productLoaded = $product->load($idProduct);
                $idVendor = $productLoaded->getCustomAttribute("hoang_tu_product_vendor")->getValue();

                $vendorObj = $this->vendorFactory->create()->load($idVendor);
                $totalSales = $vendorObj->getTotalSales();
                $totalSales=$totalSales+$qtyShipped;
                $vendorObj->setTotalSales($totalSales);
                $vendorObj->save();
            }
        }
    }
}
