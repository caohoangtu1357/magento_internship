<?php

namespace Magenest\JuniorChap6\Controller\Adminhtml\Test;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magenest\JuniorChap6\Api\TestRepositoryInterface;
use Magento\Ui\Component\MassAction\Filter;
use Magenest\JuniorChap6\Model\ResourceModel\Test\CollectionFactory;

class MassDelete extends Action{

    private $filter;
    private $collectionFactory;
    private $testRepository;

    public function __construct(
        Action\Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        TestRepositoryInterface $testRepository

    )
    {
        $this->testRepository = $testRepository;
        $this->collectionFactory = $collectionFactory;
        $this->filter=$filter;
        parent::__construct($context);
    }

    public function execute()
    {
        $collections = $this->filter->getCollection($this->collectionFactory->create())->load();
        $a=$collections->getSize();

        foreach ($collections as $collection){
            $this->testRepository->delete($collection->getData('id'));
        }

        $this->messageManager->addSuccess("Total of $a deleted");

        $redirect = $this->resultFactory->create("redirect");
        return $redirect->setPath("*/*/");

    }
}
