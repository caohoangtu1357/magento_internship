<?php

namespace Packt\HelloWorld\Block;
use Magento\Framework\View\Element\Template;

class Testplugin extends Template{

    public function __construct(
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
    }

    public function sum($a , $b){
        return $a+$b;
    }

    public function sub($a,$b){
        return $a-$b;
    }
}
