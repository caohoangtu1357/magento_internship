<?php
namespace Packt\HelloWorld\Model;

class Cron{

    protected $collectionFactory;
    protected $logger;

    public function __construct(
        \Packt\HelloWorld\Model\SubscriptionFactory $collectionFactory,
        \Psr\Log\LoggerInterface $logger
    ){
        $this->collectionFactory=$collectionFactory;
        $this->logger=$logger;
    }

    public function checkSubscriptions(){
        $collection = $this->collectionFactory->create();
        $collection->setFirstname("Cron");
        $collection->setLastname("Job");
        $collection->setEmail("cron.job@example.com");
        $collection->setMessage("Created from cron");
        $collection->save();
        $this->logger->debug('Registered Event pa hon');

    }
}
