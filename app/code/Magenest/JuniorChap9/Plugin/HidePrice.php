<?php

namespace Magenest\JuniorChap9\Plugin;

class HidePrice{
    public function aroundGetPrice($subject,$proceed){
        if($subject->getSpecialPrice()){
            $to = ($subject->getSpecialToDate());
            if($today = date("Y-m-d H:i:s") < $to){
                return "Special: ".$subject->getData()['name'];
            }

        }
        return $proceed();
    }
}
