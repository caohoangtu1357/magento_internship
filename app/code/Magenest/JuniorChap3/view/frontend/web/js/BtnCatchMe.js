define([
    'jquery',
    'jquery/ui'
],function($){
    $.widget('mage.ButtonCatchMe',{
        options:{
        },
        _create:function(){
            this._bind();
        },
        _bind: function(){
            this._on({"hover": this._hover});
        },
        _hover:function(){
            var min = Math.ceil(20);
            var max = Math.floor(100);
            var top =Math.floor(Math.random() * (max - min + 1)) + min;
            var bottom = Math.floor(Math.random() * (max - min + 1)) + min;
            var left = Math.floor(Math.random() * (max - min + 1+150)) + min+150;
            var right = Math.floor(Math.random() * (max - min + 1+150)) + min+150;

            this.element.css({marginTop:top+"px",marginBottom:bottom+"px",marginRight:right+"px",marginLeft:left+"px"});
        }

    });

    return $.mage.ButtonCatchMe;
})
