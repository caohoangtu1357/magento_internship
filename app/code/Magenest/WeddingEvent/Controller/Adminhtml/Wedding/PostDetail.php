<?php

namespace Magenest\WeddingEvent\Controller\Adminhtml\Wedding;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magenest\WeddingEvent\Model\WeddingEventFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Controller\ResultFactory;

class PostDetail extends Action{

    private $weddingEventFactory;
    private $productFactory;

    public function __construct(
        Action\Context $context,
        WeddingEventFactory $weddingEventFactory,
        ProductFactory $productFactory
    )
    {
        $this->productFactory=$productFactory;
        $this->weddingEventFactory=$weddingEventFactory;
        parent::__construct($context);
    }

    public function createProduct($postValue){
        $product = $this->productFactory->create();
        $product->setWebsiteIds(array(1));
        $product->setSku($postValue["title"]);
        $product->setName($postValue["title"]);
        $product->setPrice($postValue["commission"]);
        $product->setStatus(1);
        $product->setAttributeSetId(4);
        $product->setTypeId(\Magento\Catalog\Model\Product\Type::TYPE_VIRTUAL);
        $product->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
        $product->setWeddingId($postValue['wedding_id']);
        $product->setTaxClassId(0);
        $product->setCategoryIds([14]);

        $product->setStockData(
            [
                'use_config_manage_stock' => 0,
                'manage_stock' => 1,
                'is_in_stock' => 1,
                'qty' => 200
            ]
        );
        $product->save();
    }

    public function execute()
    {

        $post=$this->getRequest()->getPostValue();
        $wedding=$this->weddingEventFactory->create();
        $wedding->setTitle($post['title']);
        $wedding->setCommission($post['commission']);
        $wedding->setBrideFirstname($post['bride_firstname']);
        $wedding->setBrideLastname($post['bride_lastname']);
        $wedding->setBrideEmail($post['bride_email']);
        $wedding->setSentToBride($post['sent_to_bride']);
        $wedding->setGroomFirstName($post['groom_firstname']);
        $wedding->setGroomLastname($post['groom_lastname']);
        $wedding->setGroomEmail($post['groom_email']);
        $wedding->setSentToGroom($post['sent_to_groom']);
        $wedding->setMessage($post['message']);
        $wedding->save();
        $post['wedding_id']=$wedding->getId();
        $this->createProduct($post);

        $this->messageManager->addSuccess(__('Save success'));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');

    }
}
