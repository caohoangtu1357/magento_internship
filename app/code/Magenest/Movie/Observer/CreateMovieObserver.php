<?php
namespace Magenest\Movie\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CreateMovieObserver implements ObserverInterface{

    public function execute(Observer $observer)
    {
        $c=$observer->getData("postValue");
        $c->setRating(0);
        return $this;
    }
}
