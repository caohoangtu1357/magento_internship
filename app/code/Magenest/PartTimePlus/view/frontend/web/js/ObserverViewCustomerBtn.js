define(['jquery','Magento_Ui/js/modal/alert'],function($,alert){
    'use strict';

    return function observerOrderId(){

        $("#btn-vew-prefix").click(function(){
            $.get("../../part_time/customer/getcustomersameprefix",function(res){
                var content=`
                            <div id="table-content">
                            <table>
                                <tr>
                                    <th>ID</th>
                                    <th>Custom Prefix</th>
                                    <th>Full name</th>
                                    <th>Email</th>
                                </tr>`;
                res.forEach(function(item,index){
                    content+=
                        `<tr>
                            <td>${item.entity_id}</td>
                            <td>${item.custom_prefix}</td>
                            <td>${item.firstname+" "+item.lastname}</td>
                            <td>${item.email}</td>
                        </tr>`
                });
                content+=`</table></div>`

                alert({
                    title:"Customer have same prefix",
                    content:content,
                    buttons: [{
                        text: 'Close',
                        class: 'action',

                        click: function () {
                            this.closeModal(true);
                        }
                    }, {
                        text:'View All Customer',
                        class: 'action view-all',
                        click: function () {
                            $.get("../../part_time/customer/getallcustomer",function(res) {
                                $("#table-content").html("");
                                var content = `
                                    <table>
                                        <tr>
                                            <th>ID</th>
                                            <th>Custom Prefix</th>
                                            <th>Full name</th>
                                            <th>Email</th>
                                        </tr>`;
                                res.forEach(function (item, index) {
                                    content +=
                                        `<tr>
                                            <td>${item.entity_id}</td>
                                            <td>${item.custom_prefix}</td>
                                            <td>${item.firstname + " " + item.lastname}</td>
                                            <td>${item.email}</td>
                                        </tr>`
                                });
                                content += `</table>`;
                                $("#table-content").html(content);
                                $(".view-all").css("display","none");
                            });



                        }
                    }]
                });
            });
        });

    }
})
