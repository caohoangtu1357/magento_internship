<?php

namespace Magenest\CountDown\Model\Config\Source;


class ClockSize implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
            ['value' => 'small', 'label' => __('Small')],
            ['value' => 'medium', 'label' => __('Medium')],
            ['value' => 'big', 'label' => __('Big')]
        ];
    }
}
