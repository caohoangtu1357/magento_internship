<?php
namespace Magenest\Staff\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class StaffLevelOption extends AbstractSource
{


    protected $_options;

    /**
     * getAllOptions
     *
     * @return array
     */


    public function getAllOptions()
    {

        if ($this->_options === null) {
            $this->_options=[];

            $this->_options=[
                [
                    'value'=>"1",
                    'label'=>"lv1"
                ],
                [
                    'value'=>"2",
                    'label'=>"lv2"
                ],
                [
                    'value'=>"3",
                    'label'=>"other"
                ]
            ];


        }
        return $this->_options;
    }
    final public function toOptionArray()
    {
        $options = $this->getAllOptions();

        foreach ($options as $option){
            $options[]=["label"=>$option["label"],"value"=>$option["value"]];
        }

        return $options;
    }
}
