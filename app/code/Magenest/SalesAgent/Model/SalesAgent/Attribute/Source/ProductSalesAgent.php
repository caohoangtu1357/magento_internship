<?php

namespace Magenest\SalesAgent\Model\SalesAgent\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Eav\Model\Entity\Attribute\Source\SourceInterface;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;


class ProductSalesAgent extends AbstractSource implements SourceInterface,OptionSourceInterface{

    private $collectionFactory;

    public function __construct(
        CollectionFactory $collectionFactory
    ){
        $this->collectionFactory=$collectionFactory;
    }

    public function getAllSalesOrder(){
        $collection = $this->collectionFactory->create();
        $collections = $collection->addFieldToSelect("*")
            ->addAttributeToFilter("sale_agent_id",["neq"=>"NULL"])
            ->getData();
        return $collections;
    }

    public function getAllOptions()
    {
        $collections = $this->getAllSalesOrder();
        $result=[];
        foreach ($collections as $collection){
            $result[]=['value'=>$collection['sku'],'label'=>$collection['sku']];
        }
        return $result;
    }
}
