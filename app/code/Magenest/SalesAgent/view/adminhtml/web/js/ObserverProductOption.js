define(['jquery'],function($){
    'use strict';
    return function handlerChangeProduct(){
        $("#product_option").change(function(){
            var sku = $("#product_option").val();
            var url =$("#report-data-url").val();
            $.post(
                url,
                {"sku":sku}
                ,function(res){
                    var sales=[];
                    console.log(res);
                    for(let i=0;i<res.length;i++){
                        sales.push(
                            `<tr>
                                <td>${res[i].order_id}</td>
                                <td>${res[i].order_item_id}</td>
                                <td>${res[i].order_item_price}</td>
                                <td>${res[i].commision_percent}</td>
                                <td>${res[i].commission_value}</td>
                            </tr>`
                        );
                    }
                    $("#table_sales_order").empty();
                    $("#table_sales_order").append(`
                        <tr>
                            <th>Order id</th>
                            <th>Order item id</th>
                            <th>Order item price</th>
                            <th>Commission percent</th>
                            <th>Commission value</th>
                        </tr>
                    `);
                    $("#table_sales_order").append(sales);
                }
            );
        })
    }
})
