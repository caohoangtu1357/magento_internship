<?php
namespace Magenest\HoangTu\Controller\Adminhtml\Vendor;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magenest\HoangTu\Model\VendorFactory;
use Magento\Framework\Controller\ResultFactory;

class Postcreate extends Action{

    private $vendorFactory;

    public function __construct(
        Action\Context $context,
        VendorFactory $vendorFactory
    )
    {
        $this->vendorFactory=$vendorFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $vendor = $this->vendorFactory->create();
        $post = $this->getRequest()->getPostValue();

        if(isset($post["id"])){
            $vendor=$vendor->load($post['id']);
        }

        $vendor->setCustomerId($post['customer_id']);
        $vendor->setFirstName($post['first_name']);
        $vendor->setLastName($post['last_name']);
        $vendor->setEmail($post['email']);
        $vendor->setCompany($post['company']);
        $vendor->setPhoneNumber($post['phone_number']);
        $vendor->setFax($post['fax']);
        $vendor->setStreet($post['street']);
        $vendor->setAddress($post['address']);
        $vendor->setCity($post['city']);
        $vendor->setPostcode($post['postcode']);
        $vendor->setCountry($post['country']);
        $vendor->save();

        $this->messageManager->addSuccess(__("Create Success"));
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $redirect->setPath("*/*/");
    }
}
