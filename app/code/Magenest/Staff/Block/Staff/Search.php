<?php
namespace Magenest\Staff\Block\Staff;

use Magento\Framework\View\Element\Template;

class Search extends Template{


    public function __construct(
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
    }

    public function getSearchUrl(){
        return $this->getUrl("staff/staff/getsearch");
    }

}
