<?php

namespace Magenest\Test\Controller\Adminhtml\TestAddress;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magenest\Test\Model\TestAddressFactory;
use Magento\Framework\Controller\ResultFactory;

class PostDetail extends Action{

    private $pageFactory;
    private $testAddressFactory;

    public function __construct(
        Action\Context $context,
        TestAddressFactory $testAddressFactory
    )
    {
        $this->testAddressFactory=$testAddressFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();

        $testAddress=$this->testAddressFactory->create();
        if(isset($post['id'])){
            $testAddress=$testAddress->load($post['id']);
        }
        $testAddress->setStreet($post['street']);
        $testAddress->setCity($post['city']);
        $testAddress->setCountry($post['country']);
        $testAddress->setRegion($post['region']);
        $testAddress->setPostcode($post['postcode']);
        $testAddress->setTelephone($post['telephone']);
        $testAddress->setFax($post['fax']);
        $testAddress->setFirstname($post['firstname']);
        $testAddress->setLastname($post['lastname']);
        $testAddress->setEmail($post['email']);
        $testAddress->save();


        $this->messageManager->addSuccess(__('Save success'));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');

    }
}
