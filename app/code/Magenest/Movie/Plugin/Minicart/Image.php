<?php

namespace Magenest\Movie\Plugin\Minicart;

use Magento\Framework\View\Element\UiComponentFactory;

class Image
{

    protected $url;
    protected $_productRepositoryFactory;
    protected $imageHelper;

    public function __construct(
        UiComponentFactory $uiComponentFactory,
        $data = [],
        \Magento\Framework\UrlInterface $url,
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $product,
        \Magento\Catalog\Helper\Image $imageHelper
    )
    {
        $this->url = $url;
        $this->_productRepositoryFactory = $product;
        $this->imageHelper = $imageHelper;
    }

    public function aroundGetItemData($subject, $proceed, $item)

    {

        $result = $proceed($item);
        $children = $item->getChildren();
        $numChildren=count($children);
        if($numChildren==0){
            return $result;
        }
        $dataChildren = $children[0]->getData();
        $c = $dataChildren["product"];
        $name = $c->getData("name");
        $price=$c->getData("price");

        $thumbnail = $c->getData("thumbnail");


        $product = $this->_productRepositoryFactory->create()->getById($c->getData("entity_id"));
        $image_url = $this->imageHelper->init($product, 'product_base_image')->getUrl();
        $result['product_image']['src'] = $image_url;
        $result['product_name'] = $name;
        $result['price']=$price;

        return $result;
    }

}
