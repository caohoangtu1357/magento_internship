define(['jquery','Magento_Ui/js/modal/alert'],function($,alert){
    'use strict';

    var getLevel=function(level){
        switch (parseInt(level)){
            case 1:
                return "(lv1)";
            case 2:
                return "(lv2)";
            default:
                return "(other)"
        }
    }

    return function changePrice(){
         $.get("staff/customer/getlevelcustomer", function (res) {

             setTimeout(function(){
                 console.log(res);
                 if (res.status == "found") {
                     console.log(res.staff.type);
                     var level = getLevel(res.staff.type);
                     var a = $(".price-wrapper .price");
                     for(let i=0;i<a.length;i++){
                         var c =a[i];
                         c.textContent=c.textContent+level;
                     }
                 } else {
                     var level = "(other)";
                     var a = $(".price-wrapper .price");
                     for(let i=0;i<a.length;i++){
                         var c =a[i];
                         c.textContent=c.textContent+level;
                     }                 }
             },3000);

        });
    }
})
