<?php
namespace Magenest\PartTimePlus\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class AddCustomPrefixToCustomer implements ObserverInterface{

    public function execute(Observer $observer)
    {
        $lengthFirstname = strlen($observer->getCustomer()->getFirstname());
        $observer->getCustomer()->setCustomPrefix($lengthFirstname%2==0?"Odd":"Even");
        return $observer;
    }
}
