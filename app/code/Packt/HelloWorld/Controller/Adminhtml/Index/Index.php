<?php
namespace Packt\HelloWorld\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action{

    protected $resultPageFactory;

    public function __construct(Action\Context $context,PageFactory $resultPageFactory)
    {
        parent::__construct($context);
        $this->resultPageFactory=$resultPageFactory;
    }

    public function execute()
    {
        $result = $this->resultPageFactory->create();
        return $result;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed("Packt_HelloWorld::index");
    }
}
