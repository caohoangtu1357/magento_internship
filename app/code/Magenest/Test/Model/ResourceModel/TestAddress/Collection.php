<?php
namespace Magenest\Test\Model\ResourceModel\TestAddress;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection{
    protected $_idFieldName="id";
    public function _construct()
    {
        $this->_init("Magenest\Test\Model\TestAddress","Magenest\Test\Model\ResourceModel\TestAddress");
    }
}
