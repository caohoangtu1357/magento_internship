<?php
namespace Packt\HelloWorld\Controller\Adminhtml\Subscription;

use Magento\Backend\App\Action;
use Packt\HelloWorld\Model\ResourceModel\Subscription\CollectionFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Ui\Component\MassAction\Filter;
//use Magento\Catalog\Model\ResourceModel\Product\Collection;
//use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Controller\ResultFactory;
class MassDelete extends \Magento\Backend\App\Action
{

    protected $filter;
    protected $collectionFactory;

    public function __construct(\Magento\Backend\App\Action\Context $context,Filter $filter,CollectionFactory $collectionFactory)
    {
        $this->filter=$filter;
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create())->load();
        $collectionSize = $collection->getSize();
        $collection->walk('delete');
        $this->messageManager->addSuccess(__('A total of %1 element(s) have been deleted.', $collectionSize));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
