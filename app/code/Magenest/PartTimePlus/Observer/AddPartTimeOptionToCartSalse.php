<?php
namespace Magenest\PartTimePlus\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Serialize\Serializer\Json;

class AddPartTimeOptionToCartSalse implements ObserverInterface{

    private $_request;
    private $json;

    public function __construct(
        RequestInterface $request,
        Json $json
    ){
        $this->json = $json;
        $this->_request = $request;
    }

    public function execute(Observer $observer)
    {
        $post = $this->_request->getPostValue();
        if(isset($post['keyword']) &&$post['keyword']!=""){
            $keyword = $post['keyword'];
            $product = $observer->getProduct();
            $additionalOptions = [];
            $additionalOptions[] = [
                'label' => "KEYWORD",
                'value' => "$keyword",
            ];
            $observer->getData()['item']->getOptions()[0]->setCode("additional_options");
            $observer->getData()['item']->getOptions()[0]->setValue($this->json->serialize($additionalOptions));

        }
        return $observer;
    }
}
