<?php
namespace Magenest\HoangTu\Controller\Vendor;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;
use Magenest\HoangTu\Model\VendorFactory;

class PostCreate extends Action{

    private $vendorFactory;
    private $sessionCustomer;

    public function __construct(
        Context $context,
        VendorFactory $vendorFactory,
        \Magento\Customer\Model\Session $sessionCustomer
    )
    {
        $this->sessionCustomer=$sessionCustomer;
        $this->vendorFactory=$vendorFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        $vendor = $this->vendorFactory->create();
        $post = $this->getRequest()->getPostValue();
        $idCustomer = $this->sessionCustomer->getCustomer()->getId();

        if(isset($post['id'])){
            $vendor->load($idCustomer,"customer_id");
        }

        $vendor->setFirstName($post['first_name']);
        $vendor->setLastName($post['last_name']);
        $vendor->setEmail($post['email']);
        $vendor->setCompany($post['company']);
        $vendor->setCountry($post['country']);
        $vendor->setPhoneNumber($post['phone_number']);
        $vendor->setPostcode($post['postcode']);
        $vendor->setFax($post['fax']);
        $vendor->setAddress($post['address']);
        $vendor->setStreet($post['street']);
        $vendor->setCity($post['city']);
        $vendor->setCustomerId($idCustomer);
        $vendor->save();

        $this->messageManager->addSuccess("Post Success");
        $redirect = $this->resultFactory->create("redirect");
        return $redirect->setPath("*/*/subscribe");

    }
}
