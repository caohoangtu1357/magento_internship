<?php
namespace Magenest\SalesAgent\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class CommissionTypeOption extends AbstractSource
{


    protected $_options;

    /**
     * getAllOptions
     *
     * @return array
     */


    public function getAllOptions()
    {

        if ($this->_options === null) {
            $this->_options=[];

            $this->_options=[
                [
                    'value'=>"fixed",
                    'label'=>"fixed"
                ],
                [
                    'value'=>"percent",
                    'label'=>"percent"
                ]
            ];


        }
        return $this->_options;
    }
    final public function toOptionArray()
    {
        $options = $this->getAllOptions();

        foreach ($options as $option){
            $options[]=["label"=>$option["label"],"value"=>$option["value"]];
        }

        return $options;
    }
}
