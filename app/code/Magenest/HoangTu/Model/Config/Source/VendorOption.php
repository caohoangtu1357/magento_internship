<?php

namespace Magenest\HoangTu\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magenest\HoangTu\Model\ResourceModel\Vendor\CollectionFactory;

class VendorOption extends AbstractSource{

    private $collectionFactory;

    public function __construct(
        CollectionFactory $collectionFactory
    ){
        $this->collectionFactory=$collectionFactory;
    }

    public function getVendors(){
        $vendorCollection= $this->collectionFactory->create();
        $vendorCollection->addFieldToSelect("*");
        $vendors = $vendorCollection->getData();
        return $vendors;
    }

    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options=[];


            $vendors=$this->getVendors();
            foreach ($vendors as $vendor){
                array_push($this->_options,[
                    'value'=>$vendor['id'],
                    'label'=>$vendor['first_name']." ".$vendor['last_name']
                ]);
            }

        }
        return $this->_options;
    }

    final public function toOptionArray()
    {
        $options=[];
        $vendors=$this->getVendors();
        foreach ($vendors as $vendor){
            array_push($options,[
                'value'=>$vendor['id'],
                'label'=>$vendor['first_name']." ".$vendor['last_name']
            ]);
        }

        return $options;
    }
}
