<?php
namespace Magenest\SalesAgent\Model\ResourceModel\SalesAgent;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection{
    protected $_idFieldName = 'entity_id';

    public function _construct()
    {
        $this->_init("Magenest\SalesAgent\Model\SalesAgent",
            "Magenest\SalesAgent\Model\ResourceModel\SalesAgent");
    }

}
