<?php
namespace Magenest\Movie\Controller\Adminhtml\Actor;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magenest\Movie\Model\ActorFactory;

class Postcreate extends \Magento\Backend\App\Action{

    private $actorFactory;
    public function __construct(Action\Context $context,ActorFactory $actorFactory)
    {
        $this->actorFactory = $actorFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $post=$this->getRequest()->getPostValue();
        $actor = $this->actorFactory->create();
        $actor->setName($post["name"]);
        $actor->save();

        $this->messageManager->addSuccess(__('Create success'));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed("Magenest_Movie::menu_magenest_actor");
    }
}
