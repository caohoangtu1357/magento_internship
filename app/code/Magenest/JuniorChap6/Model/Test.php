<?php

namespace Magenest\JuniorChap6\Model;
use Magenest\JuniorChap6\Api\Data\TestInterface;
use Magenest\JuniorChap6\Api\TestRepositoryInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel;

class Test extends AbstractModel implements TestInterface{



    public function __construct(
        Context $context,
        \Magento\Framework\Registry $registry,
        ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function _construct()
    {
        $this->_init("Magenest\JuniorChap6\Model\ResourceModel\Test");
    }

    public function getTest()
    {
        return $this->getData(self::TEST);
    }

    public function setTest($str)
    {
        $this->setData(self::TEST,$str);
    }
}
