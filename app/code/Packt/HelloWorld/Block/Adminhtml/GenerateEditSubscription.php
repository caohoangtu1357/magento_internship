<?php


namespace Packt\HelloWorld\Block\Adminhtml;

use Magento\Backend\Block\Template;
use Magento\Framework\Registry;
use Packt\HelloWorld\Model\ResourceModel\Subscription\CollectionFactory;

class GenerateEditSubscription extends Template
{
    public $registry;
    public $collectionFactory;

    public function __construct(Template\Context $context,Registry $registry,CollectionFactory $collectionFactory, array $data = [])
    {
        $this->registry=$registry;
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context, $data);
    }

    public function getUrlAdmin()
    {
        return $this->getUrl("helloworld/subscription/update");
    }

    public function getEditRecord(){
        $id = $this->registry->registry('id');
        $collection = $this->collectionFactory->create()->addFieldToSelect('*')->addFieldToFilter('subscription_id',$id);
        $record = $collection->getData();
        $firstRecord=$record[0];
        return $firstRecord;
    }
}
