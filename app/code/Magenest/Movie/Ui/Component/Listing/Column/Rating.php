<?php
namespace Magenest\Movie\Ui\Component\Listing\Column;

use \Magento\Sales\Api\OrderRepositoryInterface;
use Magenest\Movie\Model\MovieFactory;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Framework\Api\SearchCriteriaBuilder;

class Rating extends Column
{
    protected $movieFactory;
    protected $_searchCriteria;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        SearchCriteriaBuilder $criteria,
        MovieFactory $movieFactory,
        array $components = [],
        array $data = [])
    {
        $this->movieFactory = $movieFactory;
        $this->_searchCriteria = $criteria;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        $movie=$this->movieFactory->create();
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {

                $movie = $movie->load($item["movie_id"]);
                $rating = $movie->getData("rating");

                switch ($rating) {
                    case 0;
                        $plenty_status = "<span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span>";
                        break;
                    case 1;
                        $plenty_status = "<span class='fa fa-star-half-o checked'></span><span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span>";
                        break;
                    case 2;
                        $plenty_status = "<span class='fa fa-star checked'></span><span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span>";
                        break;
                    case 3;
                        $plenty_status = "<span class='fa fa-star checked'></span><span class='fa fa-star-half-o checked'></span><span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span>";
                        break;
                    case 4;
                        $plenty_status = "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span>";
                        break;
                    case 5;
                        $plenty_status = "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star-half-o checked'></span><span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span>";
                        break;
                    case 6;
                        $plenty_status = "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star uncheck'></span><span class='fa fa-star uncheck'></span>";
                        break;
                    case 7;
                        $plenty_status = "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star-half-o checked'></span><span class='fa fa-star uncheck'></span>";
                        break;
                    case 8;
                        $plenty_status = "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star uncheck'></span>";
                        break;
                    case 9;
                        $plenty_status = "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star-half-o checked'></span>";
                        break;
                    case 10;
                        $plenty_status = "<span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span><span class='fa fa-star checked'></span>";
                        break;
                }

                // $this->getData('name') returns the name of the column so in this case it would return export_status
                $item[$this->getData('name')] = html_entity_decode($plenty_status);
            }
        }

        return $dataSource;
    }
}
