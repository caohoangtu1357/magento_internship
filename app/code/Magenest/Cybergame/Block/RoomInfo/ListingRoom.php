<?php
namespace Magenest\Cybergame\Block\RoomInfo;

use Magento\Framework\View\Element\Template;
use Magenest\Cybergame\Model\ResourceModel\RoomExtraOption\CollectionFactory;
use Magento\Eav\Model\ConfigFactory;

class ListingRoom extends Template{

    private $collectionFactory;
    private $configFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        CollectionFactory $collectionFactory,
        ConfigFactory $configFactory
    )
    {
        $this->configFactory=$configFactory;
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context, $data);
    }

    public function getPriceAttributeId(){
        $config = $this->configFactory->create();
        $id=$config->getAttribute(\Magento\Catalog\Model\Product::ENTITY, \Magento\Catalog\Api\Data\ProductInterface::PRICE)->getDataByKey("attribute_id");
        return $id;
    }

    public function getListRoom(){

        $attributeId = $this->getPriceAttributeId();


        $rooms=$this->collectionFactory->create()
            ->addFieldToSelect("*")->join(
                "catalog_product_entity_decimal",
                "main_table.product_id = catalog_product_entity_decimal.entity_id and catalog_product_entity_decimal.attribute_id=$attributeId",
                ["price"=>"catalog_product_entity_decimal.value"]
            )->getData();
        return $rooms;
    }

    public function getEditUrl(){
        return $this->getUrl("cybergame/roominfo/editroom");
    }

}
