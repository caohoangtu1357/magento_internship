<?php

namespace Magenest\SalesAgent\Block\SalesAgent;

use Magento\Framework\View\Element\Template;
use Magenest\SalesAgent\Model\ResourceModel\SalesAgent\CollectionFactory;
use Magento\Customer\Model\Session;
use Magento\Catalog\Model\ProductFactory;

class SalesAgentInfo extends Template{

    private $collectionFactory;
    private $collectionProductFactory;
    private $customerSession;
    private $productFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        CollectionFactory $collectionFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionProductFactory,
        Session $customerSession,
        ProductFactory $productFactory
    )
    {
        $this->productFactory=$productFactory;
        $this->customerSession=$customerSession;
        $this->collectionProductFactory=$collectionProductFactory;
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context, $data);
    }

    public function getSalesAgentProducts(){
        $product=$this->productFactory->create();
        $idCustomer = $this->customerSession->getCustomer()->getId();
        $salesAgents = $this->collectionProductFactory->create()
            ->addAttributeToSelect("*")
            ->addAttributeToFilter("sale_agent_id",$idCustomer)
            ->getData();

        $products=[];
        foreach ($salesAgents as $saleAgent){
            $productObj = $product->load($saleAgent['entity_id']);
            $urlProduct=$productObj->getProductUrl();
            $productData=$productObj->getOrigData();
            $productData["url"]=$urlProduct;

            $products[]=$productData;
        }

        return $products;
    }
}
