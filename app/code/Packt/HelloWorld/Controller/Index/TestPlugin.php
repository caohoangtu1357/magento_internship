<?php
namespace Packt\HelloWorld\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;

class TestPlugin extends \Magento\Framework\App\Action\Action {

    private $configData;
    protected $page;

    public function __construct(Context $context,\Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->page=$pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $renPage=$this->page->create();
        return $renPage;
    }
}
