<?php
namespace Magenest\Movie\Controller\Adminhtml\Movie;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magenest\Movie\Model\MovieFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;

class Postcreate extends \Magento\Backend\App\Action{

    private $movieFactory;
    private $eventManager;

    public function __construct(
        Action\Context $context,
        MovieFactory $movieFactory,
        EventManager $eventManager
    )
    {
        $this->eventManager=$eventManager;
        $this->movieFactory = $movieFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $post=$this->getRequest()->getPostValue();

        $postValue = new \Magento\Framework\DataObject($post);

//        $this->eventManager->dispatch("create_movie_before",["postValue"=>$postValue]);

        $movie = $this->movieFactory->create();

        if($postValue->getRating()<=0 || $postValue->getRating()>10){
            $this->messageManager->addError(__('Rating must greater than 0 and less or equal 10'));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath($this->_redirect->getRefererUrl());
        }

        $movie->setName($postValue->getName());
        $movie->setDescription($postValue->getDescription());
        $movie->setRating($postValue->getRating());
        $movie->setDirectorId($postValue->getDirectorId());
        $movie->save();

        $this->messageManager->addSuccess(__('Create success'));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed("Magenest_Movie::config_magenest_movie");
    }
}
