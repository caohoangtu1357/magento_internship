<?php
namespace Magenest\Cybergame\Block\RoomInfo;

use Magento\Framework\View\Element\Template;
use Magenest\Cybergame\Model\ResourceModel\RoomExtraOption\CollectionFactory;
use Magento\Directory\Model\CurrencyFactory;


class RoomInfoProductView extends Template{

    private $collectionFactory;
    private $currencyFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        CollectionFactory $collectionFactory,
        CurrencyFactory $currencyFactory
    )
    {
        $this->currencyFactory=$currencyFactory;
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context, $data);
    }

    public function getCurrencySymbol(){
        $symbol = $this->currencyFactory->create()->getCurrencySymbol();
        return $symbol;
    }

    public function getValidateAccountUrl(){
        return $this->getUrl("cybergame/roominfo/validateaccount");
    }


    public function getRoomDetail(){
        $collection = $this->collectionFactory->create();
        $productId = $this->getRequest()->getParam("id");

        $collection->addFieldToFilter("product_id",$productId)->addFieldToSelect("*");
        $extraOption = $collection->getData();
        if(count($extraOption)==0){
            return null;
        }
        return $extraOption[0];
    }
}
