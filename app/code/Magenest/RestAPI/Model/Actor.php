<?php

namespace Magenest\RestAPI\Model;

use Magento\Framework\Model\Context;
use Magenest\RestAPI\Api\Data\ActorInterface;

class Actor extends \Magento\Framework\Model\AbstractModel implements  ActorInterface{

    public function __construct(Context $context, \Magento\Framework\Registry $registry,\Magento\Framework\Model\ResourceModel\AbstractResource  $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = [])
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function _construct()
    {
        $this->_init("Magenest\Movie\Model\ResourceModel\Actor");
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getData("name");
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        return $this->setData("name",$name);
    }

    /**
     * {@inheritdoc}
     */
    public function get($id)
    {
        return $this->load($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getId(){
        return $this->getData("actor_id");
    }

    public function setId($id){
        return $this->setActorId("$id");
    }
}
