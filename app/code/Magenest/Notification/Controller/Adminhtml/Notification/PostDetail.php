<?php

namespace Magenest\Notification\Controller\Adminhtml\Notification;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magenest\Notification\Model\NotificationFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Magento\Customer\Model\CustomerFactory;

class PostDetail extends Action{

    private $notificationFactory;
    protected $resultFactory;
    private $customerCollectionFactory;
    private $customerModelFactory;

    public function __construct(
        CustomerFactory $customerModelFactory,
        Action\Context $context,
        NotificationFactory $notificationFactory,
        ResultFactory $resultFactory,
        CollectionFactory $customerCollectionFactory
    )
    {
        $this->customerModelFactory = $customerModelFactory;
        $this->customerCollectionFactory=$customerCollectionFactory;
        $this->resultFactory=$resultFactory;
        $this->notificationFactory=$notificationFactory;
        parent::__construct($context);
    }


    public function pushNewIdToArrayString($arr,$item){
        $arrData = explode(",",$arr);
        array_push($arrData,$item);
        return implode(",",$arrData);
    }


    public function addNotificationToAllCustomer($notificationId){
        $customer = $this->customerCollectionFactory->create();
        $customers = $customer->addFieldToSelect("entity_id")->getData();
        foreach ($customers as $idCustomer){
            $customerModel = $this->customerModelFactory->create()->load($idCustomer['entity_id']);

            $notificationReceived=$customerModel->getNotificationReceived();
            if($notificationReceived!=null){
                $notificationReceived = $this->pushNewIdToArrayString($notificationReceived,$notificationId);
            }else{
                $notificationReceived=$notificationId;
            }

            $customerModel->setNotificationReceived($notificationReceived);
            $customerModel->save();
        }

    }

    public function execute()
    {
        $post=$this->getRequest()->getPostValue();
        $notification=$this->notificationFactory->create();
        if(isset($post['entity_id'])){
            $notification->load($post['entity_id']);
        }
        $notification->setName($post['name']);
        $notification->setStatus($post['status']);
        $notification->setShortDescription($post['short_description']);
        $notification->setRedirectUrl($post['redirect_url']);
        $notification->save();

        if(!isset($post["entity_id"]) && $post["status"]==1){
            $notificationId = $notification->getEntityId();
            $this->addNotificationToAllCustomer($notificationId);
        }


        $this->messageManager->addSuccess(__('Save success'));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');

    }
}
