<?php
namespace Magenest\Test\Block\TestAddress;

use Magento\Framework\View\Element\Template;
use Magenest\Test\Model\ResourceModel\TestAddress\CollectionFactory;
class Edit extends Template{

    private $collectionFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context, $data);
    }

    public function getAddressDetail(){
        $id = $this->getRequest()->getParam("id");
        $addressCollection = $this->collectionFactory->create();
        $address =$addressCollection->addFieldToSelect("*")
            ->addFieldToFilter("id",$id)
            ->getData();
        return $address[0];
    }



    public function getSubmitUrl(){
        return $this->getUrl("*/*/postedit");
    }
}
