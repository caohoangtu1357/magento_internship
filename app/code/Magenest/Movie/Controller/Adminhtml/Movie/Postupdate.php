<?php
namespace Magenest\Movie\Controller\Adminhtml\Movie;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magenest\Movie\Model\MovieFactory;

class Postupdate extends \Magento\Backend\App\Action{

    private $movieFactory;
    public function __construct(Action\Context $context,MovieFactory $movieFactory)
    {
        $this->movieFactory = $movieFactory;
        parent::__construct($context);
    }

    public function execute()
    {
//        $post=$this->getRequest()->getPostValue();
//        $movie = $this->movieFactory->create();
//        $movie->setName($post["name"]);
//        $movie->setDescription($post["description"]);
//        $movie->setRating($post["rating"]);
//        $movie->setDirectorId($post["director_id"]);
//        $movie->save();

        $post=$this->getRequest()->getPostValue();
        $movieModel = $this->movieFactory->create();
        $movie = $movieModel->load($post["movie_id"]);
        $movie->setName($post["name"]);
        $movie->setDescription($post["description"]);
        $movie->setRating($post["rating"]);
        $movie->setDirectorId($post["director_id"]);
        $movie->save();


        $this->messageManager->addSuccess(__('Create success'));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed("Magenest_Movie::config_magenest_movie");
    }
}
