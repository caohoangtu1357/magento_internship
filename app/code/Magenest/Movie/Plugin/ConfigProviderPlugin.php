<?php
namespace Magenest\Movie\Plugin;
class ConfigProviderPlugin extends \Magento\Framework\Model\AbstractModel
{
    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject, array $result)
    {
        $items = $result['totalsData']['items'];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        for($i=0;$i<count($items);$i++){

            $quoteId = $items[$i]['item_id'];
            $quoteNext = ($quoteId + 1);

            $quote = $objectManager->create('\Magento\Quote\Model\Quote\Item')->load($quoteNext);
            $simpleProName = $quote->getName();

            $items[$i]['childname'] = $simpleProName;
        }
        $result['totalsData']['items'] = $items;
        return $result;
    }

}
