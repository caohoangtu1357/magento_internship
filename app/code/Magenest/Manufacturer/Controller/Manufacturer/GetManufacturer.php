<?php
namespace Magenest\Manufacturer\Controller\Manufacturer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;

class GetManufacturer extends Action {

    private $manufacturerFactory;
    private $jsonFactory;
    private $productFactory;

    public function __construct(
        Context $context,
        \Magenest\Manufacturer\Model\ManufacturerFactory $manufacturerFactory,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory

    )
    {
        $this->productFactory=$productFactory;
        $this->jsonFactory=$jsonFactory;
        $this->manufacturerFactory=$manufacturerFactory;
        parent::__construct($context);
    }

    public function getManufacturerId(){
        $id = $this->getRequest()->getParam('id');
        $product = $this->productFactory->create()->load($id);
        $id=$product->getOrigData('manufacturer_id');
        return $id;
    }

    public function execute()
    {
        $manufacturerID=$this->getManufacturerId();


        $manufacturerModel= $this->manufacturerFactory->create();
        $manufacturer =$manufacturerModel->load($manufacturerID);

        $result=$this->jsonFactory->create();
        $result->setData($manufacturer->getData());

        return $result;
    }
}
