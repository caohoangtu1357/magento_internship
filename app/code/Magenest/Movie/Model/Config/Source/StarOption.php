<?php
namespace Magenest\Movie\Model\Config\Source;


class StarOption implements \Magento\Framework\Option\ArrayInterface{



    public function toOptionArray()
    {
        $options=[];
        for($i=1;$i<=10;$i++){
            $options[]=["label"=>$i." star","value"=>$i];
        }
        return $options;
    }
}
