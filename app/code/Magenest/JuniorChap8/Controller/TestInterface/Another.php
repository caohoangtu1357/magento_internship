<?php

namespace Magenest\JuniorChap8\Controller\TestInterface;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magenest\JuniorChap8\Api\MyInterface;

class Another extends Action{

    private $my;

    public function __construct(
        MyInterface $my,
        Context $context

    )
    {
        $this->my=$my;
        parent::__construct($context);
    }


    public function execute()
    {
        $this->my->foo();
        // TODO: Implement execute() method.
    }
}
