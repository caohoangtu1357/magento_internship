<?php
namespace Magenest\JuniorChap6\Block\Test;

use Magento\Framework\View\Element\Template;

class TestBlock extends Template{

    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
        $this->setTemplate('Magenest_JuniorChap6::testBlock2.phtml');

    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

    }
}
