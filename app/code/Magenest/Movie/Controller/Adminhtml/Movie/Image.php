<?php
namespace Magenest\Movie\Controller\Adminhtml\Movie;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;
use Magenest\Movie\Model\ImageUploader;
use Magento\Customer\Model\FileUploader;

class Image extends \Magento\Backend\App\Action
{

    private $imageUploader;
    private $fileUploader;

    public function __construct(
        Action\Context $context,
        ImageUploader $imageUploader
//        FileUploader $fileUploader
    )
    {
//        $this->fileUploader=$fileUploader;
        $this->imageUploader=$imageUploader;
        parent::__construct($context);
    }

    public function execute()
    {
        $a=$this->imageUploader;
        $result = $this->imageUploader->saveFileToTmpDir('avatar');
        $result['cookie'] = [
            'name' => $this->_getSession()->getName(),
            'value' => $this->_getSession()->getSessionId(),
            'lifetime' => $this->_getSession()->getCookieLifetime(),
            'path' => $this->_getSession()->getCookiePath(),
            'domain' => $this->_getSession()->getCookieDomain(),
        ];

        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed("Magenest_Movie::config_magenest_movie");
    }
}
