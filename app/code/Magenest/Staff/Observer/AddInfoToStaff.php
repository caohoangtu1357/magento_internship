<?php
namespace Magenest\Staff\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magenest\Staff\Model\StaffFactory;

class AddInfoToStaff implements ObserverInterface{

    private $staffFactory;

    public function __construct(
        StaffFactory $staffFactory
    ){
        $this->staffFactory=$staffFactory;
    }

    public function execute(Observer $observer)
    {
        $customerData= $observer->getCustomer()->getData();
        if(isset($customerData["staff_type"])){
            $staff = $this->staffFactory->create();

            $isInserted = $staff->load($customerData['entity_id'],"customer_id");
            if(count($isInserted->getData())!=0){
                return $observer;
            }

            $staff->setCustomerId($customerData['entity_id']);
            $staff->setNickName($customerData['firstname']." ".$customerData["lastname"]);
            $staff->setType($customerData["staff_type"]);
            $staff->setStatus(2);
            $staff->setUpdateAt(date("Y-m-d h:i:sa"));
            $staff->save();
        }

        return $observer;
    }
}
