<?php

namespace Magenest\Manufacturer\Model;

use Magento\Framework\Model\Context;

class Manufacturer extends \Magento\Framework\Model\AbstractModel {

    public function __construct(Context $context, \Magento\Framework\Registry $registry,\Magento\Framework\Model\ResourceModel\AbstractResource  $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = [])
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function _construct()
    {
        $this->_init("Magenest\Manufacturer\Model\ResourceModel\Manufacturer");
    }
}
