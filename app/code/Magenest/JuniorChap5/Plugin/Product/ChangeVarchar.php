<?php
namespace Magenest\JuniorChap5\Plugin\Product;

class ChangeVarchar{
    public function beforeSave($product){
        $typeVarchar = $product->getTypeVarchar();

        $product->setTypeVarchar($typeVarchar." varchar(".strlen($typeVarchar).")");
        return $product;
    }

    public function afterLoad($subject, $product){
        $product->setTypeVarchar(explode(" varchar(",$product->getTypeVarchar())[0]);
        return $product;
    }
}
