<?php

namespace Magenest\Movie\Model\ResourceModel;

class Director extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb{

    protected function _construct()
    {
        $this->_init("magenest_director","director_id");
    }

    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        return parent::load($object, $value, $field);
    }

}
