<?php
namespace Magenest\PartTimePlus\Block\Question;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Model\ProductFactory;

class ShowQuestion extends Template{

    private $productFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        ProductFactory $productFactory
    )
    {
        $this->productFactory=$productFactory;
        parent::__construct($context, $data);
    }

    public function reverseQuestion($question){
        $quesReverse="";
        if(strlen($question)>5){
            for($i=4;$i>=0;$i--){
                $quesReverse.=$question[$i];
            }
        }else{
            for($i=strlen($question);$i>=0;$i--){
                $quesReverse.=$question[$i];
            }
        }
        return $quesReverse;

    }

    public function getProductQuestion(){
        $idProduct = $this->getRequest()->getParam("id");
        $product = $this->productFactory->create();
        $question = $product->load($idProduct)->getQuestion();
        return $this->reverseQuestion($question);
    }

}
