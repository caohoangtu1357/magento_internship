<?php
namespace Magenest\JuniorChap6\Model;

use Magenest\JuniorChap6\Api\Data\TestInterface;
use Magenest\JuniorChap6\Api\TestRepositoryInterface;
use Magenest\JuniorChap6\Model\TestFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;

class TestRepositoryModel implements TestRepositoryInterface{

    private $testFactory;
    private $manager;

    public function __construct(
        TestFactory $testFactory,
        EventManager $manager
    ){
        $this->manager = $manager;
        $this->testFactory = $testFactory;
    }

    public function save(TestInterface $test)
    {
        $this->manager->dispatch("save_test_before",["test"=>$test]);
        $testObj = $this->testFactory->create();
        return $testObj->getResource()->save($test);
    }

    public function getById($id)
    {
        $testObj = $this->testFactory->create();
        return $testObj->load($id);
    }

    public function delete($id)
    {
        $testObj = $this->testFactory->create();
        return $testObj->load($id)->delete();
    }
}
