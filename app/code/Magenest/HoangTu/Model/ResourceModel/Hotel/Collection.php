<?php
namespace Magenest\HoangTu\Model\ResourceModel\Hotel;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection{
    protected $_idFieldName="hotel_id";
    public function _construct()
    {
        $this->_init("Magenest\HoangTu\Model\Hotel","Magenest\HoangTu\Model\ResourceModel\Hotel");
        parent::_construct();
    }
}
