<?php
namespace Magenest\JuniorChap3\Model\Config\Source;

use Magento\Framework\Data\Form\Element\AbstractElement;


use Magento\Framework\View\Element\Html\Select;

class Color extends \Magento\Framework\View\Element\AbstractBlock
{


    public function setName($elementName)
    {
        $this->setData('name', $elementName);
        return $this;
    }

    /**
     * Set element's HTML ID
     *
     * @param string $elementId ID
     * @return $this
     */

    public function setId($elementId)
    {
        $this->setData('id', $elementId);
        return $this;
    }

    /**
     * Set element's CSS class
     *
     * @param string $class Class
     * @return $this
     */
    public function setClass($class)
    {
        $this->setData('class', $class);
        return $this;
    }

    /**
     * Set element's HTML title
     *
     * @param string $title Title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->setData('title', $title);
        return $this;
    }

    /**
     * HTML ID of the element
     *
     * @return string
     */
    public function getId()
    {
        return $this->getData('id');
    }

    /**
     * CSS class of the element
     *
     * @return string
     */
    public function getClass()
    {
        return $this->getData('class');
    }

    /**
     * Returns HTML title of the element
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->getData('title');
    }

    /**
     * Render HTML
     *
     * @return string
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _toHtml()
    {
        if (!$this->_beforeToHtml()) {
            return '';
        }

        $id = $this->getInputId();

        $html= '<script type="text/javascript">
            require(["jquery","jquery/colorpicker/js/colorpicker"], function ($) {
                $(document).ready(function () {
                	var $el = $("#'.$this->getInputId().'");
                    $color = $el.css("backgroundColor", $el.val());
                    $el.ColorPicker({
                    	color: "$color",
                        onChange: function (hsb, hex, rgb) {
                            $el.css("backgroundColor", "#" + hex).val("#" + hex);
                    	}
                	});
            	});
        	});
         </script>';
        $html.="<input name='".$this->getInputName()."' type='text' id='".$this->getInputId()."' class='ddg-colpicker'/>";
        return $html;
    }



}
