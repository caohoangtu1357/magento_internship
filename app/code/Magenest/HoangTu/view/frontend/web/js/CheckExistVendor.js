define(['jquery'],function($,alert){
    return function handlerButtonGetInfo(){

        setTimeout(function(){
            $("#email").change(function(){
                var email = $("#email").val();
                var urlSubmit = $("#submit_url_ajax").val()+"email/"+email;

                $.get(urlSubmit,function(res){
                    if(res.status=="found"){
                        $("#alertEmail").css("display","block");
                    }else{
                        $("#alertEmail").css("display","none");
                    }

                });
            });
        },3000);

    }
})
