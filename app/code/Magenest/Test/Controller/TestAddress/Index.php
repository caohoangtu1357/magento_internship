<?php

namespace Magenest\Test\Controller\TestAddress;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action{

    private $pageFactory;

    public function __construct(
        Context $context,
        PageFactory $pageFactory
    )
    {
        $this->pageFactory=$pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $page=$this->pageFactory->create();
        $page->getConfig()->getTitle()->prepend("Address management");
        return $page;
    }
}
