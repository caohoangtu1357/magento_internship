<?php


namespace Magenest\Movie\Block\Adminhtml\Testbackend;
use Magento\Backend\Block\Template;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class Otherinfo extends Template
{
    private $invoiceRepository;
    private $searchCriteriaBuilder;
    private $colectionFactory;
    private $orderFactory;
    private $customerFactory;
    private $creditFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        InvoiceRepositoryInterface $invoiceRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CollectionFactory $colectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderFactory,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory,
        \Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory $creditFactory
    )
    {
        $this->orderFactory=$orderFactory;
        $this->colectionFactory=$colectionFactory;
        $this->searchCriteriaBuilder=$searchCriteriaBuilder;
        $this->invoiceRepository=$invoiceRepository;
        $this->customerFactory=$customerFactory;
        $this->creditFactory=$creditFactory;
        parent::__construct($context, $data);
    }

    public function getUrlAdmin(){
        return $this->getUrl("helloworld/subscription/create");
    }

    public function getTotalInvoice(){
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $listInvoice = $this->invoiceRepository->getList($searchCriteria);
        $count=$listInvoice->getTotalCount();
        return $count;
    }

    public function getTotalProduct(){
        $productCollection=$this->colectionFactory->create();
        $collection = $productCollection->addFieldToSelect("*");
        $products = $collection->getData();
        $count=count($products);
        return $count;
    }

    public function getTotalOrder(){
        $order = $this->orderFactory->create();
        $orderObj = $order->addFieldToSelect("*");
        $orders = $orderObj->getData();
        $count=count($orders);
        return $count;
    }

    public function getTotalCustomer(){
        $customer = $this->customerFactory->create();
        $customerObj = $customer->addFieldToSelect("*");
        $customers=$customerObj->getData();
        $count= count($customers);
        return $count;
    }

    public function getTotalCredit(){

        $credit = $this->creditFactory->create();
        $creditObj = $credit->addFieldToSelect("*")->addFieldToFilter("created_at",date("Y-m-d"));
        $credits = $creditObj->getData();
        $count= count($credits);
        return $count;
    }
}
