<?php

namespace Magenest\JuniorChap10\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;

use Magento\Framework\Setup\ModuleContextInterface;

use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface

{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $setup->startSetup();


            $tableName = $setup->getTable('catalog_product_entity');

            if ($setup->getConnection()->isTableExists($tableName) == true) {

                $columns = [

                    'tok_value' => [

                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,

                        'nullable' => true,

                        'comment' => 'Token Value',

                    ],

                ];

                $connection = $setup->getConnection();

                foreach ($columns as $name => $definition) {

                    $connection->addColumn($tableName, $name, $definition);

                }


            }




        $setup->endSetup();

    }

}
