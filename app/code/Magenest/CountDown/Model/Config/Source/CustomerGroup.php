<?php

namespace Magenest\CountDown\Model\Config\Source;

use Magento\Framework\View\Element\Html\Select;
use Magento\Customer\Model\ResourceModel\Group\CollectionFactory;

class CustomerGroup extends Select
{
    private $collectionFactory;

    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        CollectionFactory $collectionFactory,
        array $data = []
    )
    {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }



    /**
     * Set "name" for <select> element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Set "id" for <select> element
     *
     * @param $value
     * @return $this
     */
    public function setInputId($value)
    {
        return $this->setId($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml(): string
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->getSourceOptions());
        }
        return parent::_toHtml();
    }


    public function getCustomerGroupOptions() : array{
        $customerGroups = $this->collectionFactory->create()->getData();
        $options = [];
        foreach ($customerGroups as $customerGroup){
            $options[] = ['label' => $customerGroup['customer_group_code'], 'value' => $customerGroup['customer_group_id']];
        }
        return $options;
    }

    private function getSourceOptions(): array
    {
        return $this->getCustomerGroupOptions();
    }
}
