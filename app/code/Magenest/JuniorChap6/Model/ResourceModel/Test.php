<?php
namespace Magenest\JuniorChap6\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Test extends AbstractDb{

    protected function _construct()
    {
        $this->_init("test_table_junior_chap6","id");
    }
}
