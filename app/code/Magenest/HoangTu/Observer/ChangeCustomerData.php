<?php
namespace Magenest\HoangTu\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magenest\HoangTu\Model\VendorFactory;

class ChangeCustomerData implements ObserverInterface{

    private $vendorFactory;

    public function __construct(
        VendorFactory $vendorFactory
    ){
        $this->vendorFactory=$vendorFactory;
    }

    public function execute(Observer $observer)
    {

        $customerData= $observer->getCustomer();

        $vendor=$this->vendorFactory->create();
        $vendor->setEmail($customerData->getEmail());
        $vendor->setFirstName($customerData->getFirstname());
        $vendor->setLastName($customerData->getLastname());
        $vendor->setCustomerId($customerData->getId());
        $vendor->save();


        return $observer;
    }
}
