<?php

namespace Packt\HelloWorld\Controller\Index;
class Event extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();

        $parameters = [
            'var1' => "value var 1",
            'var2' => "value var 2"
        ];
        $this->_eventManager ->dispatch('helloworld_register_visit', $parameters);

        return $resultPage;
    }
}
