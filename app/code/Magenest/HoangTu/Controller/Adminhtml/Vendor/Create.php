<?php
namespace Magenest\HoangTu\Controller\Adminhtml\Vendor;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;

class Create extends Action{

    private $pageFactory;

    public function __construct(
        Action\Context $context,
        PageFactory $pageFactory
    )
    {
        $this->pageFactory=$pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $page=$this->pageFactory->create();
        $page->getConfig()->getTitle()->prepend("Create Vendor");
        return $page;
    }
}
