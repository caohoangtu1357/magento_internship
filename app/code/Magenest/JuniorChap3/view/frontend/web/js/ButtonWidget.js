define([
    'jquery',
    'jquery/ui'
],function($){
    $.widget('mage.ButtonWidget',{
        options:{
            color:"green",
            width:"200px"
        },
        _create:function(){
            console.log(this.element);
            this.element.css({"color":this.options.color});
            this.element.css({"width":this.options.width});
            this._bind();
        },
        _bind: function(){
            this._on({"click": this._click});
        },
        _click:function(){
            var min = Math.ceil(0);
            var max = Math.floor(5);
            var textRandom=["Hello","Hi friend","Anonymous","Mr.Robot","Romeo","Juliet"]
            this.element.text(textRandom[Math.floor(Math.random() * (max - min + 1)) + min]);
        }
    });

    return $.mage.ButtonWidget;
})
