<?php
namespace Packt\HelloWorld\Model\Config\Source;

class Custom implements \Magento\Framework\Option\ArrayInterface{

    public function toOptionArray()
    {
        return [
            "custom 1"=>"custom 1",
            "custom 2"=>"custom 2"
        ];
    }
}
