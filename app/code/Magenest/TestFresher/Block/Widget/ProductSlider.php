<?php
namespace Magenest\TestFresher\Block\Widget;
use Magento\Framework\View\Element\Template;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Pricing\Helper\Data;

class ProductSlider extends Template implements \Magento\Widget\Block\BlockInterface {

    private $collectionFactory;
    private $productFactory;
    private $productRepositoryInterfaceFactory;
    private $imageHelper;
    private $productRepository;
    private $priceHelper;

    public function __construct(
        Template\Context $context,
        array $data = [],
        CollectionFactory $collectionFactory,
        ProductFactory $productFactory,
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryInterfaceFactory,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        Data $priceHelper
    )
    {
        $this->priceHelper = $priceHelper;
        $this->productRepository = $productRepository;
        $this->imageHelper = $imageHelper;
        $this->productRepositoryInterfaceFactory = $productRepositoryInterfaceFactory;
        $this->productFactory = $productFactory;
        $this->collectionFactory= $collectionFactory;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('Magenest_TestFresher::widget/productslider.phtml');
    }

    public function getProducts(){
        $product = $this->collectionFactory->create();
        $product = $product->addFieldToSelect("*")->getData();
        return $product;
    }

    public function getPriceAndImage($id){
        $product = $this->productRepositoryInterfaceFactory->create()->getById($id);
        $price = $this->priceHelper->currency($product->getFinalPrice(), true, false);
        $image = $this->imageHelper->init($product,'product_base_image')->getUrl();
        return ["image"=>$image,"price"=>$price];
    }

    public function getMediaUrl(){
        $mediaUrl = $this ->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA );
        return $mediaUrl;
    }

}
