<?php
namespace Packt\HelloWorld\Controller\Index;

use Magento\Framework\App\ResponseInterface;

class Collection extends \Magento\Framework\App\Action\Action {

    public function execute()
    {
        $productColection = $this->_objectManager->create("Magento\Catalog\Model\ResourceModel\Product\Collection")
            ->addAttributeToSelect(['name','price'])
            ->addAttributeToFilter('name', array( 'like'=>"%Rc%"))
            ->addAttributeToFilter('entity_id',array('in'=>['1','2','3']))
            ->setPageSize(10,1);
        $output="";

        //return query to string
        $productColection->getSelect()->__toString();
//
//        //update data
//        $productColection->setDataToAll("price",20);
//        //save to database
//        $productColection->save();

        foreach ($productColection as $product){
            $output.= var_dump($product->debug(),null,false);
        }
        $this->getResponse()->setBody($output);
    }
}
