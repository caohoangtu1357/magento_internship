<?php
namespace Magenest\SalesAgent\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\PatchInterface;
use Magento\Eav\Setup\EavSetupFactory;

class CommissionTypeProduct implements \Magento\Framework\Setup\Patch\DataPatchInterface {

    private $eavSetupFactory;
    private $moduleDataSetup;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->moduleDataSetup = $moduleDataSetup;
    }

    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'commission_type',
            [
                'type' => 'varchar',
//                'frontend' => '',
                'label' => "Commission type",
                'input' => 'select',
                'class' => '',
                'source' => 'Magenest\SalesAgent\Model\Config\Source\CommissionTypeOption',
                'backend' => '',
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => ''
            ]
        );
        $this->moduleDataSetup->getConnection()->endSetup();
    }


    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }
}
