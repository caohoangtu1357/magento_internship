<?php
namespace Packt\HelloWorld\Controller\Index;

use Magento\Framework\App\ResponseInterface;

class Subscription extends \Magento\Framework\App\Action\Action{

    public function execute()
    {
        $subscription = $this->_objectManager->create("Packt\HelloWorld\Model\Subscription");
        $subscription->setFirstname("Tu");
        $subscription->setLastname("Hoang");
        $subscription->setMessage("My message");
        $subscription->setEmail("123@gmail.com");
        $subscription->save();
        $this->getResponse()->setBody("success");
    }
}
