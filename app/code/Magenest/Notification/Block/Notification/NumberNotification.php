<?php
namespace Magenest\Notification\Block\Notification;

use Magento\Framework\View\Element\Template;
use Magento\Customer\Model\SessionFactory;
use Magento\Customer\Model\CustomerFactory;
use Magenest\Notification\Model\ResourceModel\Notification\CollectionFactory;

class NumberNotification extends Template{

    private $customerSessionFactory;
    private $customerFactory;
    private $collectionFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        SessionFactory $customerSessionFactory,
        CustomerFactory $customerFactory,
        CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->customerFactory=$customerFactory;
        $this->customerSessionFactory=$customerSessionFactory;
        parent::__construct($context, $data);
    }

    public function getIdCustomer(){
        $customerSession = $this->customerSessionFactory->create();
        $id = $customerSession->getCustomer()->getId();
        return $id;
    }

    public function getNotificationExistInDb($ids){
        $notification = $this->collectionFactory->create();
        $notifications = $notification->addFieldToSelect("*")->addFieldToFilter("entity_id",["in"=>$ids])
            ->getData();
        return count($notifications);
    }

    public function getNumberNotification(){
        $idCustomer = $this->getIdCustomer();

        $customer = $this->customerFactory->create()->load($idCustomer);

        $numNotificationReceived = $customer->getNotificationReceived()?explode(",",$customer->getNotificationReceived()):[];
        $numNotificationViewed = $customer->getNotificationViewed()?explode(",",$customer->getNotificationViewed()):[];
        return $this->getNotificationExistInDb($numNotificationReceived)- $this->getNotificationExistInDb($numNotificationViewed);
    }
}
