<?php
namespace Magenest\HoangTu\Plugin\Customer;

use Magenest\HoangTu\Model\VendorFactory;

class BeforeSave{

    private $vendorFactory;

    public function __construct(
        VendorFactory $vendorFactory
    ){
        $this->vendorFactory=$vendorFactory;
    }

    public function beforeSave($input){

        $dataInput=$input->getData();
        $vendor=$this->vendorFactory->create();
        $vendor->setEmail($dataInput['email']);
        $vendor->setFirstname($dataInput['firstname']);
        $vendor->setLastname($dataInput['lastname']);
        $vendor->save();

        $input->setData("hoang_tu_is_approved",0);
        return $input;
    }
}
