<?php
namespace Magenest\HoangTu\Model\ResourceModel\Vendor;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection{
    protected $_idFieldName="id";
    public function _construct()
    {
        $this->_init("Magenest\HoangTu\Model\Vendor","Magenest\HoangTu\Model\ResourceModel\Vendor");
    }
}
