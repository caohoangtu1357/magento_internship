<?php
namespace Magenest\RestAPI\Api;


/**
 *
 * @api
 */
interface ActorRepositoryInterface{


    /**
     * update Actor
     *
     * @return Data\ActorInterface
     */
    public function createActor();


    /**
     * get Actor by id
     *
     *
     * @return Data\ActorInterface
     */
    public function get();


    /**
     * update Actor
     *
     * @return Data\ActorInterface
     */
    public function update();

    /**
     * @param int $id
     * @return string
     */
    public function deleteById($id);
}
