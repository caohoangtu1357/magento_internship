<?php
namespace Magenest\Movie\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ChangeCustomerFirstName implements ObserverInterface{

    public function execute(Observer $observer)
    {
        $cus=$observer->getCustomer()->setData("firstname","Magenest");
        return $this;
    }
}
