<?php
namespace Magenest\HoangTu\Block\Vendor;

use Magento\Framework\View\Element\Template;

class VendorForm extends Template{
    private $sessionCustomer;
    private $collectionFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        \Magento\Customer\Model\Session $sessionCustomer,
        \Magenest\HoangTu\Model\ResourceModel\Vendor\CollectionFactory $collectionFactory

    )
    {
        $this->collectionFactory=$collectionFactory;
        $this->sessionCustomer=$sessionCustomer;
        parent::__construct($context, $data);
    }

    public function getSubmitUrl(){
        return $this->getUrl("hoang_tu/vendor/postcreate");
    }
    public function getAjaxUrl(){
        return $this->getUrl("hoang_tu/vendor/CheckExistVendor");
    }
    public function getFormInfor(){
        $idCustomer = $this->sessionCustomer->getCustomer()->getId();
        $collection=$this->collectionFactory->create();
        $collection->addFieldToSelect("*")->addFieldToFilter("customer_id",$idCustomer);
        $data=$collection->getData();
        return $data[0];
    }
}
