define(
    [
        'ko',
        'uiComponent',
        'underscore',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/quote',
        'jquery'
    ],
    function (
        ko,
        Component,
        _,
        stepNavigator,
        customer,
        quote,
        $
    ) {
        'use strict';
        /**
         * check-login - is the name of the component's .html template
         */
        return Component.extend({
            defaults: {
                template: 'Magenest_JuniorChap10/check-login'
            },

            //add here your logic to display step,
            isVisible: ko.observable(quote.isVirtual()),
            isLogedIn: customer.isLoggedIn(),
            //step code will be used as step content id in the component template
            stepCode: 'login',
            //step title value
            stepTitle: 'Login',
            email:ko.observable(""),
            password: ko.observable(),

            /**
             *
             * @returns {*}
             */
            initialize: function () {
                this._super();

                var data = JSON.parse(localStorage['mage-cache-storage']);
                this.email(data["checkout-data"].inputFieldEmailValue);

                // if(!customer.isLoggedIn()){
                    stepNavigator.registerStep(
                        //step code will be used as step content id in the component template
                        this.stepCode,
                        //step alias
                        null,
                        //step title value
                        this.stepTitle,
                        //observable property with logic when display step or hide step
                        this.isVisible,

                        _.bind(this.navigate, this),

                        /**
                         * sort order value
                         * 'sort order value' < 10: step displays before shipping step;
                         * 10 < 'sort order value' < 20 : step displays between shipping and payment step
                         * 'sort order value' > 20 : step displays after payment step
                         */
                        0
                    );
                // }
                // register your step



                return this;
            },

            /**
             * The navigate() method is responsible for navigation between checkout step
             * during checkout. You can add custom logic, for example some conditions
             * for switching to your custom step
             */
            navigate: function () {

                this.isVisible(true);
            },


            /**
             * @returns void
             */
            navigateToNextStep: function () {
                var self = this;
                $.ajax({
                    type: 'POST',
                    url: '../customer/ajax/login',
                    context: this,
                    data:
                        JSON.stringify({
                            username:$("#username").val(),
                            password:$("#password").val(),
                            context:"checkout",
                            form_key:$("input[name=form_key]").val()
                        })
                        ,
                    success: function(res) {

                        if(res.errors==false){
                            window.location.replace(window.location.href.replace("#login","#shipping"));
                            window.location.reload();
                        }else{
                            var data = JSON.parse(localStorage['mage-cache-storage']);
                            data["checkout-data"].inputFieldEmailValue=$("#username").val();
                            data["checkout-data"].validatedEmailValue="";
                            data["checkout-data"].inputFieldEmailValue=$("#username").val();
                            localStorage['mage-cache-storage']=JSON.stringify(data);
                            window.location.replace(window.location.href.replace("#login","#shipping"));
                            // window.location.reload();
                        }

                    }
                });

            }
        });
    }

);


















define(
    [   'jquery',
        'Magento_Checkout/js/view/payment/default',
        'ko',
        'uiComponent',
        'underscore',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/totals',
        'Magento_Checkout/js/model/quote'
    ],
    function (
        $,
        payment,
        ko,
        Component,
        _,
        stepNavigator,
        customer,
        totals,
        quote
    ) {

        return Component.extend({
            defaults: {
                template: 'Junior_Chap7/footer'
            },
            initialize: function (){
                this._super();
                if (totals.totals()) {
                    this.subtotal=ko.observable((parseFloat(totals.totals()['grand_total'])));
                }
                $(function() {
                    $('#co-shipping-method-form').change(function () {
                        console.log(1);
                    });
                });
                $(function() {
                    $('body').on("click", '#place-order-trigger', function () {
                        $(".payment-method._active").find('.action.primary.checkout').trigger( 'click' );
                    });
                });
            },
            goToPrevStep: function () {
                if(window.location.hash=='#login')
                    window.location.replace("http://example.com/checkout/cart/");
                else if(window.location.hash=='#shipping')
                    stepNavigator.navigateTo('login');
                else if(window.location.hash=='#payment')
                    stepNavigator.navigateTo('shipping');
            },
            goToNextStep: function () {
                if(quote.shippingMethod() && quote.totals())
                    this.subtotal(parseFloat(quote.totals()['subtotal'])+parseFloat(quote.shippingMethod()['amount']));
                if(window.location.hash=='#shipping') {
                    $('#submit-btn').click();
                } else
                if(window.location.hash=='#payment') {
                    $('#place-order-trigger').click();
                }
                else stepNavigator.next();
            }
        })
    });
