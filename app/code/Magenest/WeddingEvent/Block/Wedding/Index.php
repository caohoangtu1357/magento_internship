<?php

namespace Magenest\WeddingEvent\Block\Wedding;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Block\Product\ListProduct;


class Index extends Template{

    private $listProduct;

    public function __construct(
        Template\Context $context,
        array $data = [],
        ListProduct $listProduct
    )
    {
        $this->listProduct=$listProduct;
        parent::__construct($context, $data);
    }

    public function getWeddingInfoUrl(){
        return $this->getUrl("weddingevent/wedding/weddinginfo");
    }

    public function getAddToCartPostParamUrl(){
        return $this->getUrl("weddingevent/wedding/addtocartpostparam");
    }

    public function getProduct(){

    }

    public function getAddToCartPostParam($product){
        return $this->listProduct->getAddToCartPostParams($product);
    }

}
