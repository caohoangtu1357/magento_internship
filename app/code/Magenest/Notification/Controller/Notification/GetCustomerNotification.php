<?php

namespace Magenest\Notification\Controller\Notification;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\SessionFactory;

class GetCustomerNotification extends Action{

    private $jsonFactory;
    private $customerFactory;
    private $notificationCollectionFactory;
    private $sessionFactory;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        CustomerFactory $customerFactory,
        \Magenest\Notification\Model\ResourceModel\Notification\CollectionFactory $notificationCollectionFactory,
        SessionFactory $sessionFactory
    )
    {
        $this->sessionFactory = $sessionFactory;
        $this->notificationCollectionFactory = $notificationCollectionFactory;
        $this->customerFactory = $customerFactory;
        $this->jsonFactory=$jsonFactory;
        parent::__construct($context);
    }

    public function getNotificationContentByIds($ids){
        $notification = $this->notificationCollectionFactory->create();
        $notifications = $notification->addFieldToSelect("*")->addFieldToFilter("entity_id",["in"=>$ids])
            ->getData();
        return $notifications;
    }

    public function getAllNotificationContent($idCustomer){
        $customer = $this->customerFactory->create()->load($idCustomer);
        $notificationReceived = $customer->getNotificationReceived();
        if($notificationReceived){
            $notificationReceiveds = explode(",",$notificationReceived);
            $notifications = $this->getNotificationContentByIds($notificationReceiveds);
            return $notifications;
        }else{
            return [];
        }
    }

    public function addIsViewedFieldToNotification($idCustomer,$allNotification){
        $customer = $this->customerFactory->create()->load($idCustomer);
        $notificationViewed = $customer->getNotificationViewed();
        if($notificationViewed){
            $notificationVieweds = explode(",",$notificationViewed);
            $notifications = $this->getNotificationContentByIds($notificationVieweds);
            $notificationViewedIds=[];
            foreach ($notifications as $notification){
                array_push($notificationViewedIds,$notification['entity_id']);
            }

            foreach ($allNotification as & $notification){
                if(in_array($notification['entity_id'],$notificationViewedIds)){
                    $notification["viewed"]=true;
                }else{
                    $notification["viewed"]=false;
                }
            }

            return $allNotification;
        }else{
            return $allNotification;
        }
    }

    public function getCustomerId(){
        return $this->sessionFactory->create()->getCustomer()->getId();
    }

    public function execute()
    {
        $idCustomer = $this->getCustomerId();
        $notifications = $this->getAllNotificationContent($idCustomer);
        $notifications= $this->addIsViewedFieldToNotification($idCustomer,$notifications);
        $jsonData = $this->jsonFactory->create();
        $jsonData->setData($notifications);
        return $jsonData;
    }
}
