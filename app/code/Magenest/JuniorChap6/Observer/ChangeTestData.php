<?php
namespace Magenest\JuniorChap6\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ChangeTestData implements ObserverInterface{

    public function execute(Observer $observer)
    {
        $observer->getTest()->setTest("test modified");
    }
}
