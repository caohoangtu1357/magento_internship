<?php
namespace Magenest\JuniorChap6\Model\ResourceModel\Test;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection{
    protected $_idFieldName = 'id';

    public function _construct()
    {
        $this->_init("Magenest\JuniorChap6\Model\Test",
            "Magenest\JuniorChap6\Model\ResourceModel\Test");
    }
}
