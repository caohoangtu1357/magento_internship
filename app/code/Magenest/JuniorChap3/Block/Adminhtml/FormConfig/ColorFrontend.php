<?php

namespace Magenest\JuniorChap3\Block\Adminhtml\FormConfig;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magenest\CountDown\Model\Config\Source\ClockType;
use Magenest\CountDown\Model\Config\Source\CustomerGroup;
use Magenest\JuniorChap3\Model\Config\Source\Color;
/**
 * Class Ranges
 */
class ColorFrontend extends AbstractFieldArray
{
    /**
     * @var Color
     */
    private $colorRenderer;

    /**
     * Prepare rendering the new field by adding all the needed columns
     */
    protected function _prepareToRender(): void
    {
        $this->addColumn('color_name', [
            'label' => __('Color name'),
            'class' => 'required-entry',
            ]);
        $this->addColumn('color_value', [
            'label' => __('Color value'),
            'class' => 'required-entry',
            'renderer' => $this->getColorRenderer()
        ]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    private function getColorRenderer()
    {
        if (!$this->colorRenderer) {
            $this->colorRenderer = $this->getLayout()->createBlock(
                Color::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->colorRenderer;
    }

}
