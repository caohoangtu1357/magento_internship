define(['jquery', 'uiComponent', 'ko'], function($, Component, ko) {
    return Component.extend({

        defaults: {
            template: 'Magenest_Movie/templatedata',
            activeMethod: ''
        },

        initialize:function(){
            this._super();

            this.input1=ko.observable("input 1");
            this.input2=ko.observable("input 2");
            console.log(this);
            this.dataInput= ko.pureComputed(function(){
                console.log(this.input1());
                return this.input1() + this.input2();
            },this);
        },
        getTemplate: function () {
            return this.template;
        }
    });
});
