<?php


namespace Magenest\Movie\Block\Adminhtml\Testbackend;
use Magento\Backend\Block\Template;

class NumberModule extends Template
{
    private $fullModuleList;

    public function __construct(
        Template\Context $context,
        array $data = [],
        \Magento\Framework\Module\FullModuleList $fullModuleList
    )
    {
        $this->fullModuleList=$fullModuleList;
        parent::__construct($context, $data);
    }

    public function getTotalModuleOfAnotherVendor(){
        $count=0;
        $allModule = $this->fullModuleList->getAll();
        foreach ($allModule as $module){
            $moduleName=$module['name'];
            if (strpos($moduleName, 'Magento_') === false) {
                $count+=1;
            }
        }
        return $count;
    }
    public function getTotalModuleOfMagento(){
        $count=0;
        $allModule = $this->fullModuleList->getAll();
        return count($allModule);
    }

}
