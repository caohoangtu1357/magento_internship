<?php
namespace Magenest\CountDown\Controller\Adminhtml\CountDown;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use Magenest\CountDown\Model\CountDownFactory;

class PostDetail extends Action{

    private $pageFactory;
    private $countDownFactory;

    public function __construct(
        CountDownFactory $countDownFactory,
        Action\Context $context
    )
    {
        $this->countDownFactory = $countDownFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $post= $this->getRequest()->getPostValue();
        $countDown = $this->countDownFactory->create();
        $countDown->setCountdownValue($post['countdown_value']);
        $countDown->setTitle($post['title']);
        $countDown->setStatus($post['status']);
        $countDown->save();
        $this->messageManager->addSuccess("Post Success");
        $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $result->setPath("*/*/");
    }
}
