<?php
namespace Magenest\Notification\Controller\Notification;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Customer\Model\SessionFactory;
use Magento\Customer\Model\CustomerFactory;

class DeleteNotification extends Action{

    private $jsonFactory;
    private $sessionFactory;
    private $customerFactory;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        SessionFactory $sessionFactory,
        CustomerFactory $customerFactory
    )
    {
        $this->customerFactory = $customerFactory;
        $this->sessionFactory = $sessionFactory;
        $this->jsonFactory=$jsonFactory;
        parent::__construct($context);
    }

    public function getIdCustomer(){
        return $this->sessionFactory->create()->getCustomer()->getId();
    }

    public function removeNote($idCustomer,$idNote){
        $customer = $this->customerFactory->create()->load($idCustomer);
        $vieweds = $customer->getNotificationViewed();
        if($vieweds){
            $viewedsArr = explode(",",$vieweds);

            if (($key = array_search($idNote, $viewedsArr)) !== false) {
                unset($viewedsArr[$key]);
            }
            $customer->setNotificationViewed(implode(",",$viewedsArr));
        }
        $receiveds = $customer->getNotificationReceived();
        if($receiveds){
            $receivedsArr = explode(",",$receiveds);

            if (($key = array_search($idNote, $receivedsArr)) !== false) {
                unset($receivedsArr[$key]);
            }
            $customer->setNotificationReceived(implode(",",$receivedsArr));
        }
        $customer->save();

    }

    public function execute()
    {
        $idNote = $this->getRequest()->getParam("id");
        $idCustomer = $this->getIdCustomer();
        $this->removeNote($idCustomer,$idNote);
        $json = $this->jsonFactory->create();
        $json->setData(["status"=>"success"]);
        return $json;
    }
}
