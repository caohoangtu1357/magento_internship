<?php

namespace Packt\HelloWorld\Controller\Adminhtml\Subscription;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;

class EditSubscription extends \Magento\Backend\App\Action{

    protected $pageFactory;
    public $registry;

    public function __construct(Action\Context $context,PageFactory $pageFactory,Registry $registry)
    {
        $this->pageFactory=$pageFactory;
        $this->registry=$registry;
        parent::__construct($context);
    }

    public function execute()
    {

        $id = $this->getRequest()->getParam('id');
        $this->registry->register('id', $id);

        $pageResult = $this->pageFactory->create();
        return $pageResult;
    }

    public function _isAllowed()
    {
        return true;
    }
}
