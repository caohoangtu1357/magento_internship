<?php
namespace Magenest\Staff\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

class StaffTypeCustomer implements DataPatchInterface{


    private $moduleDataSetup;

    private $customerSetupFactory;

    private $attributeSetFactory;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->attributeSetFactory=$attributeSetFactory;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    public function apply()
    {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);


        $customerSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'staff_type',
            [
                'type' => 'int',
                'label' => 'Staff type',
                'frontend'=>'',
                'input' => 'select',
                'source'=>'Magenest\Staff\Model\Config\Source\StaffLevelOption',
                'required' => false,
                'sort_order' => 30,
                'visible' => true,
                'user_defined' => true,
                'unique' => false,
                'system' => false,
                'visible_on_front' => false,
                'is_used_in_grid'=>0
            ]
        );

        $attribute = $customerSetup->getEavConfig()->getAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'staff_type'
        );

        $attribute->addData(
            [
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => [
                    'adminhtml_customer'
                ],
            ]
        );
        $attribute->setData('used_in_forms',[
            'adminhtml_customer'
        ]);


        $attribute->save();
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

}
