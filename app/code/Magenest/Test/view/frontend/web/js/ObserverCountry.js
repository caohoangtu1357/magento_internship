define(['jquery'],function($){
    'use strict';
    return function handlerChangeCountry(){

        $.get("https://restcountries.eu/rest/v2/all",function(res){
            var selected_country=$("#selected_country").val();

            var countries=[];
            if(selected_country==""){
                countries.push("<option disabled selected>Choose country</option>")
            }
            res.forEach(function(country,index){
                if(selected_country == country.name){
                    countries.push(`<option value="${country.name}" region="${country.region}" selected>${country.name}</option>`)
                }else{
                    countries.push(`<option value="${country.name}" region="${country.region}" >${country.name}</option>`)
                }

            });
            $('#country').append(countries);
        });

        $('#country').change(function(){
            $("#region").val($('#country').find(":selected").attr("region"));
        });
    }
})
