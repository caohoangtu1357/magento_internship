<?php
namespace Magenest\HoangTu\Ui\Component\Listing\Column\Order;

use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Model\OrderFactory;
use Magento\Catalog\Model\ProductFactory;

class ProductName extends Column
{
    protected $_orderRepository;
    protected $_searchCriteria;
    protected $orderFactory;
    protected $productFactory;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $criteria,
        array $components = [],
        OrderFactory $orderFactory,
        ProductFactory $productFactory,
        array $data = [])
    {
        $this->productFactory=$productFactory;
        $this->orderFactory=$orderFactory;
        $this->_orderRepository = $orderRepository;
        $this->_searchCriteria = $criteria;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {

        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as $keyOrder=> & $item) {

                $order = $this->orderFactory->create()->load($item['entity_id']);
                $orderItems = $order->getAllVisibleItems();
                $productName = [];
                foreach ($orderItems as $key=> $itemOrder){
                    $itemData = $itemOrder->getData();
                    $itemName=$itemData['name'];
                    if(substr($itemData['name'],0,1)=="c"||substr($itemData['name'],0,1)=="C"){
                        $itemData['name']=html_entity_decode("<p style='color: red'>$itemName</p>");
                    }
                    array_push($productName,$itemData['name']);
                }
                $dataSource['data']['items'][$keyOrder]['product_name']=implode( ", ", $productName );

            }
        }

        return $dataSource;
    }
}
