<?php

namespace Magenest\JuniorChap3\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Color extends  AbstractHelper{

    const XML_PATH = "junior_chap_3/";

    private function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    public function getConfigColor($code, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH .'corlor_frontend_config_page/'. $code, $storeId);
    }
}
