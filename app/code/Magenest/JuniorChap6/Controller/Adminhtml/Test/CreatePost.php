<?php
namespace Magenest\JuniorChap6\Controller\Adminhtml\Test;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Magenest\JuniorChap6\Api\TestRepositoryInterface;
use Magenest\JuniorChap6\Api\Data\TestInterface;

class CreatePost extends Action{

    private $testRepository;
    private $test;

    public function __construct(
        Action\Context $context,
        TestRepositoryInterface $testRepository,
        TestInterface $test
    )
    {
        $this->test = $test;
        $this->testRepository = $testRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();

        if(isset($post['id'])){
            $this->test = $this->testRepository->getById($post['id']);
        }

        $this->test->setTest($post['test']);
        $this->testRepository->save($this->test);

        $this->messageManager->addSuccess("Post Successfully");
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $redirect->setPath("*/*/");

    }
}
