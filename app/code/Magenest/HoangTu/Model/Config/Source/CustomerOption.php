<?php

namespace Magenest\HoangTu\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;

class CustomerOption implements ArrayInterface{

    private $collectionFactory;

    public function __construct(
        CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory=$collectionFactory;
    }

    public function toOptionArray()
    {
        $customerCollection = $this->collectionFactory->create();
        $customerCollection->addFieldToSelect("*");
        $customers = $customerCollection->getData();

        $options=[];
        foreach ($customers as $customer){
            $options[]=["label"=>$customer['firstname']." ". $customer['lastname'],"value"=>$customer["entity_id"]];
        }

        return $options;
    }
}
