<?php
namespace Magenest\Movie\Model\Config\Source;

class ShowOption implements \Magento\Framework\Option\ArrayInterface{

    public function toOptionArray()
    {
        return [
            1=>"show",
            2=>"not-show"
        ];
    }
}
