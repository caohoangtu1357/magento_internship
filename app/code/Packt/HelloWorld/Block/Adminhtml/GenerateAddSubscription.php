<?php


namespace Packt\HelloWorld\Block\Adminhtml;
use Magento\Backend\Block\Template;

class GenerateAddSubscription extends Template
{
    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }

    public function getUrlAdmin(){
        return $this->getUrl("helloworld/subscription/create");
    }
}
