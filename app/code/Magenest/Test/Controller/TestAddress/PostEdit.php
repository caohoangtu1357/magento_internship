<?php
namespace Magenest\Test\Controller\TestAddress;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Magenest\Test\Model\TestAddressFactory;

class PostEdit extends Action {

    private $pageFactory;
    private $testAddressFactory;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        TestAddressFactory $testAddressFactory
    )
    {
        $this->testAddressFactory=$testAddressFactory;
        $this->pageFactory=$pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        $testAddress=$this->testAddressFactory->create();
        $testAddress=$testAddress->load($post['id']);
        $testAddress->setStreet($post['street']);
        $testAddress->setCity($post['city']);
        $testAddress->setCountry($post['country']);
        $testAddress->setRegion($post['region']);
        $testAddress->setPostcode($post['postcode']);
        $testAddress->setTelephone($post['telephone']);
        $testAddress->setFax($post['fax']);
        $testAddress->setFirstname($post['firstname']);
        $testAddress->setLastname($post['lastname']);
        $testAddress->setEmail($post['email']);
        $testAddress->save();


        $this->messageManager->addSuccess(__('Save success'));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
