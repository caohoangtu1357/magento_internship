<?php

namespace Magenest\Movie\Controller\Adminhtml\Customer\Image;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

class Upload extends \Magento\Backend\App\Action{
    protected $imageUploader;
    public function __construct(
        Action\Context $context,
        \Magento\Catalog\Model\ImageUploader $imageUploader
    )
    {
        $this->imageUploader=$imageUploader;
        parent::__construct($context);
    }

    public function execute()
    {
        $result=$this->imageUploader->saveFileToTmpDir("customer[image]");
        $result['cookie'] = [
            'name' => $this->_getSession()->getName(),
            'value' => $this->_getSession()->getSessionId(),
            'lifetime' => $this->_getSession()->getCookieLifetime(),
            'path' => $this->_getSession()->getCookiePath(),
            'domain' => $this->_getSession()->getCookieDomain(),
        ];
        $a=$result;
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}
