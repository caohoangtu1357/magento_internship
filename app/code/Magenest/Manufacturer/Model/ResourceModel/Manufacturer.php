<?php

namespace Magenest\Manufacturer\Model\ResourceModel;

class Manufacturer extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb{

    protected function _construct()
    {
        $this->_init("manufacturer_entity","entity_id");
    }

    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        return parent::load($object, $value, $field);
    }

}
