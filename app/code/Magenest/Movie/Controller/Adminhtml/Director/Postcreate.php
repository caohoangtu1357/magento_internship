<?php
namespace Magenest\Movie\Controller\Adminhtml\Director;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magenest\Movie\Model\DirectorFactory;

class Postcreate extends \Magento\Backend\App\Action{

    private $directorFactory;
    public function __construct(Action\Context $context,DirectorFactory $directorFactory)
    {
        $this->directorFactory = $directorFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $post=$this->getRequest()->getPostValue();
        $director = $this->directorFactory->create();
        $director->setName($post["name"]);
        $director->save();

        $this->messageManager->addSuccess(__('Create success'));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed("Magenest_Movie::menu_magenest_director");
    }
}
