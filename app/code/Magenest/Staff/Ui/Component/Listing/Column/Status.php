<?php
namespace Magenest\Staff\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class Status extends Column{
    public function __construct(ContextInterface $context, UiComponentFactory $uiComponentFactory, array $components = [], array $data = [])
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        $a=$dataSource;
        foreach ($dataSource['data']['items'] as & $item){
            switch ($item['status']){
                case 2:
                    $item['status']="approved";
                    break;
                default:
                    $item['status']="pending";
            }
        }
        return $dataSource;
    }
}
