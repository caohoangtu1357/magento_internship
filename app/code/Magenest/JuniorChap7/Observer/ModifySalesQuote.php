<?php
namespace Magenest\JuniorChap7\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Serialize\Serializer\Json;

class ModifySalesQuote implements ObserverInterface{

    private $_request;
    private $json;

    public function __construct(
        RequestInterface $request,
        Json $json
    ){
        $this->json = $json;
        $this->_request = $request;
    }

    public function execute(Observer $observer)
    {
        $product = $observer->getProduct();
        $additionalOptions = [];
        $t=time();
        $additionalOptions[] = [
            'label' => "timestamp",
            'value' => "$t",
        ];
        $observer->getData()['item']->getOptions()[0]->setCode("additional_options");
        $observer->getData()['item']->getOptions()[0]->setValue($this->json->serialize($additionalOptions));


        return $observer;
    }
}
