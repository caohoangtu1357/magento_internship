<?php
namespace Magenest\Test\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Request\Http;

class AddPrefixAddress implements ObserverInterface{

    protected $_request;

    public function __construct(
        RequestInterface $request
    ) {
        $this->_request = $request;
    }

    public function getPrefixCountry($countryName){
        $prefix="";
        if(strpos($countryName," ")){
            $words = explode(" ",$countryName);
            $prefix=$words[count($words)-1];
        }else{
            for($i=0;$i<strlen($countryName);$i++){
                if($i<3){
                    $prefix=$prefix.$countryName[$i];
                }else{
                    break;
                }
            }
        }
        return $prefix;
    }

    public function execute(Observer $observer)
    {
        $address = $observer->getData()['object'];
        $prefix=$this->getPrefixCountry($address->getCountry());
        $address->setPrefix($prefix);
        return $observer;
    }
}
