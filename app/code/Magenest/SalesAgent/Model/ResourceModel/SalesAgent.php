<?php
namespace Magenest\SalesAgent\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class SalesAgent extends AbstractDb{

    protected function _construct()
    {
        $this->_init("magenest_sales_agent","entity_id");
    }

    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        return parent::load($object, $value, $field);
    }
}
