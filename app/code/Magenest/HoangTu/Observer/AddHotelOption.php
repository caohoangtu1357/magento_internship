<?php
namespace Magenest\HoangTu\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magenest\HoangTu\Model\VendorFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\App\Request\Http;
use Magenest\HoangTu\Model\HotelFactory;

class AddHotelOption implements ObserverInterface{

    protected $_request;
    private $json;
    private $hotelFactory;

    public function __construct(
        RequestInterface $request,
        Json $json,
        HotelFactory $hotelFactory
    ) {
        $this->hotelFactory=$hotelFactory;
        $this->json=$json;
        $this->_request = $request;
    }

    public function execute(Observer $observer)
    {
        $post = $this->_request->getPostValue();
        if(isset($post['hotel_id'])){
            $idHotel = $post['hotel_id'];
            $roomType = $post['roomtype'];

            $hotel = $this->hotelFactory->create()->load($idHotel);
            $qty=$hotel->getData("available_".$roomType);
            $hotelId=$hotel->getData("hotel_id");
            $hotelName=$hotel->getData("hotel_name");

            if ($this->_request->getFullActionName() == 'checkout_cart_add') { //checking when product is adding to cart
                $product = $observer->getProduct();
                $additionalOptions = [];
                if($qty>0){
                    $additionalOptions[] = [
                        'label' => "Hotel id ",
                        'value' => "$idHotel",
                    ];
                }else{
                    $additionalOptions[] = [
                        'label' => "Hotel name $hotelName",
                        'value' => "Invisible",
                    ];
                }

                $observer->getProduct()->addCustomOption('additional_options', $this->json->serialize($additionalOptions));
            }
        }

        return $observer;
    }
}
