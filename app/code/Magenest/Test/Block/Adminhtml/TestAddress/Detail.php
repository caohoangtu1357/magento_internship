<?php

namespace Magenest\Test\Block\Adminhtml\TestAddress;

use Magento\Backend\Block\Template;
use Magenest\Test\Model\ResourceModel\TestAddress\CollectionFactory;

class Detail extends Template{

    private $collectionFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context, $data);
    }

    public function getAddressDetail(){
        $id = $this->getRequest()->getParam("id");

        if(isset($id)){
            $address = $this->collectionFactory->create();
            $addressData = $address->addFieldToSelect('*')->addFieldToFilter("id",$id)->getData();
            return $addressData[0];
        }else{
            return null;
        }
    }

    public function getSubmitUrl(){
        return $this->getUrl("*/*/postdetail");
    }

}
