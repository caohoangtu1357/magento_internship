<?php
namespace Magenest\PartTimePlus\Controller\Customer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Magento\Customer\Model\SessionFactory;
use Magento\Customer\Model\CustomerFactory;

class GetAllCustomer extends Action{

    private $jsonFactory;
    private $collectionFactory;
    private $sessionFactory;
    private $customerFactory;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        CollectionFactory $collectionFactory,
        SessionFactory $sessionFactory,
        CustomerFactory $customerFactory
    )
    {
        $this->sessionFactory=$sessionFactory;
        $this->collectionFactory = $collectionFactory;
        $this->jsonFactory=$jsonFactory;
        $this->customerFactory= $customerFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        $customerCollection = $this->collectionFactory->create();
        $customersSanePrefix = $customerCollection
            ->addAttributeToFilter("custom_prefix",["neq"=>"NULL"])
            ->addAttributeToSelect("custom_prefix")
            ->addFieldToSelect("*")->getData();

        $jsonRes = $this->jsonFactory->create()->setData($customersSanePrefix);
        return $jsonRes;
    }
}
