<?php
namespace Magenest\Movie\Block\Adminhtml\Config\Frontend;

use Magento\Framework\Data\Form\Element\AbstractElement;

class RowInMovie extends \Magento\Config\Block\System\Config\Form\Field
{
    protected function _getElementHtml(AbstractElement $element)
    {
        $element->setDisabled('disabled');
        return $element->getElementHtml();
    }
}
