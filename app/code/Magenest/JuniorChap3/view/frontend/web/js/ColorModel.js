define(['jquery', 'uiComponent', 'ko'], function($, Component, ko) {

    var color =function(item) {
        this.color_name = ko.observable(item.color_name);
        this.color_value = ko.observable(item.color_value);

        return this;
    };

    return Component.extend({

        initialize:function(){
            this._super();
            this.colors = ko.observableArray();
            this.selected_color = ko.observable(0);
        },

        getTemplate: function () {
            return this.template;
        },


        changeSelect: function (data){
            $('body').css("backgroundColor",this.selected_color());
        },

        getColors:function(){
            var self=this;
            $.ajax({
                type: 'GET',
                url: 'color/frontendcolor/getallcolor',
                context: this,
                success: function(res) {
                    var colorOptions = [];

                    for(var prop in res){
                        colorOptions.push(new color(res[prop]));
                    }
                    self.colors(colorOptions);
                },
                dataType: 'json'
            });
        }
    });
});
