<?php
namespace Magenest\Staff\Controller\Adminhtml\Staff;

use Magento\Backend\App\Action;
use Magenest\Staff\Model\ResourceModel\Staff\CollectionFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\ResultFactory;
use Magenest\Staff\Model\StaffFactory;

class MassStatus extends \Magento\Backend\App\Action
{

    protected $filter;
    protected $collectionFactory;
    protected $staffFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        StaffFactory $staffFactory
    )
    {
        $this->staffFactory=$staffFactory;
        $this->filter=$filter;
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $staff = $this->staffFactory->create();
        $collection = $this->filter->getCollection($this->collectionFactory->create())->load();
        $collectionSize = $collection->getSize();


        foreach ($collection->getItems() as $item){
            switch ($item->getData()['status']){
                case 2:
                    $item->setStatus(3);
                    $item->save();
                    break;
                case 3:
                    $item->setStatus(2);
                    $item->save();
                    break;
            }
        }
        $this->messageManager->addSuccess(__('A total of %1 element(s) have been change status.', $collectionSize));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
